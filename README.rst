Artificial Intelligence (AI) Simulation
+++++++++++++++++++++++++++++++++++++++

Welcome to the Artifical Intelligence (AI) simulation project. This application is a basic graphical simulation of two artificial agents doing things
using AI techniques. Each agent will have be using AI behaviours and a path finding technique to perform it's tasks. This is for the AI subject
assignment.