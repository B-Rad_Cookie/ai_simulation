/*-----------------------------------------------------------------------------
file:			Graph.cpp
author:			Brad Cook
date modified:	28/08/2015
description:	This source file contains the definition of the Graph class:
				which models a graph with nodes and edges. It also extends the
				definitions for the functors used for graph operations.
-----------------------------------------------------------------------------*/

#include "Graph.h"
#include <algorithm>

/*=============================================================================
Graph::Node class.
*/

// Empty Constructor
Graph::Node::Node()
{
	// Do nothing
}

// Copy Constructor
Graph::Node::Node(const Node &a_otherNode)
{
	// Copy the other node's co-ordinate information.
	m_x = a_otherNode.m_x;
	m_y = a_otherNode.m_y;
}

// X,Y float co-ordinate
Graph::Node::Node(const int a_x, const int a_y)
{
	// Set the X and Y co-ordinates.
	m_x = a_x;
	m_y = a_y;
}
// Destructor
Graph::Node::~Node()
{
	// Do nothing
}

// Equivalency opertor overload. Tests whether two nodes are the same.
bool Graph::Node::operator==(const Node &a_otherNode)
{
	// Return true if the x and y co-ordinates of the two nodes match
	return (m_x == a_otherNode.m_x && m_y == a_otherNode.m_y);
}

// Non-Equivalency opertor overload. Tests whether two nodes are the
// same.
bool Graph::Node::operator!=(const Node &a_otherNode)
{
	// Return true if either of the co-ordinates don't match.
	return (m_x != a_otherNode.m_x || m_y != a_otherNode.m_y);
}

/*=============================================================================
Graph class.
*/

// Empty Constructor
Graph::Graph()
{
	// Set the is directed to false.
	m_isDirected = false;
}

// IsDirected Constructor 
Graph::Graph(const bool a_isDirected)
{
	// Set the isBiDirectional to the value passed in.
	m_isDirected = a_isDirected;
}

// Destructor.
Graph::~Graph()
{
	// Call the Destroy function.
	Destroy();
}

// Frees up the memory used by the Graph instance.
void Graph::Destroy()
{
	// Go through each node in the node list and free up the memory. Then clear
	// the vector.
	for (auto &node : m_lNodes)
	{
		delete node;
	}
	m_lNodes.clear();

	// Do the same for the edges. As the nodes have been freed up, no need to
	// free up the start and end nodes.
	for (auto &edge : m_lEdges)
	{
		delete edge;
	}
	m_lEdges.clear();
}

// Determines whether the graph is bi-directional or not.
bool Graph::IsDirected() const
{
	// Return the is directed variable.
	return m_isDirected;
}

// Adds a Node at the X,Y co-ordinates a_xcoord and a_ycoord. Returns the
// pointer to the created Node object.
Graph::Node* Graph::AddNode(int a_xcoord, int a_ycoord)
{
	// Create a new node and set it's position.
	Graph::Node* newNode = new Graph::Node();
	newNode->m_x = a_xcoord;
	newNode->m_y = a_ycoord;

	// Add it to the node list, and return the pointer reference.
	m_lNodes.push_back(newNode);
	return newNode;
}

// Finds and returns the node at the X, Y co-ordinate position a_xcoord and
// a_ycoord. Returns nullptr if it can not be found.
Graph::Node* Graph::FindNode(int a_xcoord, int a_ycoord) const
{
	// Make a temp node out of the co-ordinates
	Graph::Node temp = Graph::Node(a_xcoord, a_ycoord);

	// Find it by doing a simple linear search and finding the match
	for (auto &node : m_lNodes)
	{
		if (*node == temp)
		{
			return node;
		}
	}
	return nullptr;
}

// Finds and returns the node nearest to the X, Y co-ordinate position
// a_xcoord and a_ycoord.
Graph::Node* Graph::FindNearestNode(
	float a_xcoord, float a_ycoord) const
{
	/*
	Find the nearest node by going through each of the nodes, finding the
	distance squared between the node and the co-ordinates, and keeping
	track of the node with the shortest distance.
	*/

	// Make the node to return;
	Graph::Node *closestNode = nullptr;

	// Keep a record of the minimum distance from the closest node.
	float minDistance = INFINITY;

	for (auto &node : m_lNodes)
	{
		// Calculate distance squared.
		float distanceSquared = (
			((a_xcoord - node->m_x) * (a_xcoord - node->m_x)) +
			((a_ycoord - node->m_y) * (a_ycoord - node->m_y)));

		if (distanceSquared < minDistance)
		{
			closestNode = node;
			minDistance = distanceSquared;
		}
	}

	return closestNode;
}

// Remove the given node a_p_Node
void Graph::RemoveNode(Node *a_p_Node)
{
	// Firstly, find all the edges that contain the node and remove them from
	// the edges list.
	// Do this by using the remove-erase idiom.
	// First, set the node to be removed.
	NodeInEdge isNode(a_p_Node);

	// Now, do the removal
	m_lEdges.erase(std::remove_if(m_lEdges.begin(), m_lEdges.end(), isNode), m_lEdges.end());

	// Then remove the node from the nodes.
	m_lNodes.erase(remove(m_lNodes.begin(), m_lNodes.end(), a_p_Node), m_lNodes.end());
}

// Connects the node a_p_NodeStart to a_p_NodeEnd with an edge with a weight
// (cost) of a_weight.
void Graph::ConnectNodes(
	Graph::Node *a_p_NodeStart,
	Graph::Node *a_p_NodeEnd,
	const float a_weight)
{
	// Create the new edge
	Graph::Edge* pNewEdge = new Graph::Edge();
	pNewEdge->m_p_startNode = a_p_NodeStart;
	pNewEdge->m_p_endNode = a_p_NodeEnd;
	pNewEdge->m_weight = a_weight;

	// Now add to the edge list.
	m_lEdges.push_back(pNewEdge);
}

// Returns a std::vector of the edges that a_p_Node connects to. It will
// return all the connections based on whether the graph is directed or
// not. i.e. if your graph is directed, it will only return edges where the
// node is the start node. If no-directed, it will return all edges.
std::vector<Graph::Edge*> Graph::FindAllConnections(Node* a_p_Node)
{
	// Setup the return vector.
	std::vector<Graph::Edge*> connections = std::vector<Graph::Edge*>();

	// Now choose the right function to call based on the graph type
	// (directed/non-directed)
	if (this->IsDirected())
	{
		this->FindDirectionalConnections(a_p_Node, connections);
	}
	else
	{
		this->FindNonDirectedConnections(a_p_Node, connections);
	}

	// Return
	return connections;
}

// Returns a std::vector of every edge that a_p_Node is part of. Used to find
// connections in Non-directed graphs.
void Graph::FindNonDirectedConnections(
	Node* a_p_Node,
	std::vector<Graph::Edge*> &a_edgeConnections)
{
	/*
	With non-directed graphs, we return any edge that contains the node:
	regardless of whether it's start or end.
	*/

	// Go through each edge. 
	// Make sure the comparisons are done on the contents of the nodes,
	// not node pointers.
	for (auto &edge : m_lEdges)
	{
		if ((*edge->m_p_startNode) == (*a_p_Node) ||
			((*edge->m_p_endNode) == (*a_p_Node)))
		{
			a_edgeConnections.push_back(edge);
		}
	}
}

// Returns a std::vector of every edge where a_p_Node is the start node. Used
// to find edges in directed graphs.
void Graph::FindDirectionalConnections(
	Node* a_p_Node,
	std::vector<Graph::Edge*> &a_edgeConnections)
{
	/*
	With directed graphs, we return an edge only when the node is the start
	node.
	*/

	// Find every connection
	this->FindNonDirectedConnections(a_p_Node, a_edgeConnections);

	// Remove the ones where a_pNode is the endnode.
	NodeIsEndNode isEndNode(a_p_Node);
	a_edgeConnections.erase(remove_if(
		a_edgeConnections.begin(), a_edgeConnections.end(), isEndNode),
		a_edgeConnections.end());
}

/*=============================================================================
NodeInEdge class
*/

// () operator overload. Returns true if a_p_edge contains the node to be
// removed.
bool NodeInEdge::operator() (const Graph::Edge *a_p_edge)
{
	if ((*a_p_edge->m_p_startNode) == (*m_p_removeNode) ||
		(*a_p_edge->m_p_endNode) == (*m_p_removeNode))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*=============================================================================
NodeIsEndNode class
*/

// () operator overload. Returns true if the node is the end node of the edge
// a_p_edge
bool NodeIsEndNode::operator() (const Graph::Edge *a_p_edge)
{
	if (*a_p_edge->m_p_endNode == *m_p_node)
	{
		return true;
	}
	else
	{
		return false;
	}
}