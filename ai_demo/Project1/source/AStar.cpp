/*-----------------------------------------------------------------------------
file:			Agent.cpp
author:			Brad Cook
date modified:	29/08/2015
description:	This cpp file contains the definition of the AStar class:
				Finds the shortest path between two nodes on a grpah using the
				A* algorithm.
-----------------------------------------------------------------------------*/

#include "AStar.h"
#include "Graph.h"
#include <list>
#include <glm/geometric.hpp>

/*=============================================================================
AStar class
*/
AStar::AStar()
{
	// Do nothing
}


AStar::~AStar()
{
	// Do nothing. Pathfinding node memory will be released every time at the
	// end of the shortest path method.
}

// Find the shortest path between two nodes using the A* algorithm. a_Graph
// is the graph, a_p_start and a_p_end are pointers to the start and end
// nodes to find a path between respectively, and a_shortestPath is the
// std::vector of Graph Node pointers which contains the shortest path.
// a_shortestPath will be empty if no path could be found.
void AStar::AStarShortestPath(
	Graph &a_Graph,
	Graph::Node *a_p_start,
	Graph::Node *a_p_end,
	std::vector<Graph::Node*> &a_shortestPath)
{
	// If shortestPath is not empty, clear it first.
	if (!a_shortestPath.empty())
	{
		a_shortestPath.clear();
	}

	// If the start and end target node are the same, return.
	if (*a_p_start == *a_p_end)
		return;

	// Dijkstra is a modification of the breadth-first search. Rather than a queue,
	// Dijkstra needs a priority queue.
	std::list<AStarNode*> openList = std::list<AStarNode*>();
	std::vector<Graph::Node*> closedList = std::vector<Graph::Node*>();

	// Firstly, create the start node, and add it to the shortest path.
	AStarNode *p_startPathNode = new AStarNode(a_p_start);
	p_startPathNode->gScore = 0;
	m_PathNodes.push_back(p_startPathNode);

	// Make a reference to the end Path Node. Will be used for building up the
	// final path.
	AStarNode *p_endPathNode = nullptr;

	// Add the start node to the openlist.
	openList.push_back(p_startPathNode);

	// Iterate while the closed
	while (!openList.empty())
	{
		// Sort the queue to make it a priority queue.
		openList.sort(SortAStarNodeAscending());

		// Get the next node
		AStarNode *p_currentPathfindingNode = openList.front();
		Graph::Node *p_currentGraphNode = (
			p_currentPathfindingNode->m_p_graphNode);

		// Check whether the current Node is the target node.
		if (*p_currentGraphNode == *a_p_end)
		{
			p_endPathNode = p_currentPathfindingNode;
			break;
		}

		// Remove the current node.
		openList.pop_front();

		// Add the current node to the closed list.
		closedList.push_back(p_currentGraphNode);

		// Find all connections to the current node.
		std::vector<Graph::Edge*> nodeConnections;
		nodeConnections = a_Graph.FindAllConnections(p_currentGraphNode);

		// Add all the connections to the openList.
		for (auto &edge : nodeConnections)
		{
			// Define both the graph end node and graph start node of the edge
			// in an array. This will be used to process both nodes in a loop.
			Graph::Node* edgeNodes[] = { edge->m_p_endNode, edge->m_p_startNode };

			// Process the edge end node, then the edge start node.
			for (auto edgeNode : edgeNodes)
			{
				// Only process if the node is not in the closed list
				// already.
				if (find(closedList.begin(), closedList.end(), edgeNode)
					== closedList.end())
				{
					// Calculate the g-score.
					float gScore = (
						(edge)->m_weight + p_currentPathfindingNode->gScore);

					// Calculate the h-score, which is the distance square
					// (Euclidean) distance to the end node. Use the glm
					// library to do this.
					// Make temporary vec2 objects out of the nodes.
					glm::vec2 currentEdgeNode = glm::vec2((float)edgeNode->m_x,
						(float)edgeNode->m_y);
					glm::vec2 endNode = glm::vec2((float)a_p_end->m_x,
						(float)a_p_end->m_y);
					// Now do the geomteric math
					glm::vec2 distance = currentEdgeNode - endNode;
					float hScore = dot(distance, distance);

					// Calculate the f-score, which is the sum of the g and h scores.
					float fScore = gScore + hScore;

					// If the Graph Node is in the openList, find that
					// node and update it's information. Otherwise, create a new
					// Pathfinding Node for it.
					std::list<AStarNode*>::iterator iNodeInOpenList = (
						find(openList.begin(), openList.end(), p_currentPathfindingNode));

					if (iNodeInOpenList == openList.end())
					{
						// Current Node has not been processed before, so
						// create and add.
						AStarNode *p_nextPathNode = (
							new AStarNode(edgeNode));

						// Update it's attributes.
						// Set it's gScore, hscore and fscore 
						p_nextPathNode->gScore = gScore;
						p_nextPathNode->hScore = hScore;
						p_nextPathNode->fScore = fScore;

						// Set the parent Node to the current Node.
						p_nextPathNode->m_p_parentPathNode = p_currentPathfindingNode;

						// Add the New Node.
						openList.push_back(p_nextPathNode);
						m_PathNodes.push_back(p_nextPathNode);
					}
					else
					{
						// The end node is in the open list. 
						// Only update it's info if the new f-score is smaller
						// than the existing.
						if (fScore < (*iNodeInOpenList)->fScore)
						{
							// Update it's details.
							(*iNodeInOpenList)->gScore = gScore;
							(*iNodeInOpenList)->hScore = hScore;
							(*iNodeInOpenList)->fScore = fScore;
							(*iNodeInOpenList)->m_p_parentPathNode = 
								p_currentPathfindingNode;
						}
					}
				}
				// Only process the start node if the graph is non-directed.
				// If not, then break the loop.
				if (a_Graph.IsDirected())
				{
					break;
				}
			}
		}
	}

	// Now, build  up the shortest path by starting at the end (current
	// Pathfinding node) and going through each parent, adding the nodes along
	// the way.

	// However, if no path to the end is found (i.e. endPathNode is still null)
	// Return.
	if (p_endPathNode == nullptr)
	{
		return;
	}
	AStarNode* p_currentPathNode = p_endPathNode;
	while (p_currentPathNode != nullptr)
	{
		Graph::Node *p_currentNode = p_currentPathNode->m_p_graphNode;
		// Add the Graph Node.
		a_shortestPath.push_back(p_currentNode);
		// Set the current pathfinding node to the parent.
		p_currentPathNode = (p_currentPathNode->m_p_parentPathNode);
	}

	// As the vector went is currently ordered end node to start node, reverse it.
	reverse(a_shortestPath.begin(), a_shortestPath.end());

	// Release all the memory created.
	Destroy();
}

// Remove all created Pathfinding Nodes
void AStar::Destroy()
{
	// Remove all the Pathfinding nodes, then clear the std::vector
	for (auto &pathnode : m_PathNodes)
	{
		delete pathnode;
	}
	m_PathNodes.clear();
}

/*===========================================================================//
SortAStarNodeAscending Class.
*/

bool SortAStarNodeAscending::operator()(
	const AStar::AStarNode* a_p_lhs,
	const AStar::AStarNode* a_p_rhs)
{
	// Return true if lsh f-score is less than rhs f-score.
	return a_p_lhs->fScore < a_p_rhs->fScore;
}