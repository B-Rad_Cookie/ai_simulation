/*-----------------------------------------------------------------------------
file:			LureBehaviours.cpp
author:			Brad Cook
description:	This cpp file contains the definitions of classes (nodes)
				used to perform luring slimes into a well behaviour as part of
				a Behaviour Tree.
-----------------------------------------------------------------------------*/

#include "LureBehaviours.h"
#include "Blobby.h"
#include "BlackSlime.h"
#include "Circle.h"
#include "EntityManager.h"
#include "Seek.h"
#include <vector>
#include <glm\geometric.hpp>

/*=============================================================================
						NearOpenWellBehvaiour Class.
=============================================================================*/

// Determines whether Blobby is within 100 pixels of an open well. If so,
// returns success, returns failure otherwise.
Status NearOpenWellBehvaiour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Go through the wells and check whether one is within distance. If one is
	// set it as the open well.
	std::vector<Well*> wells = a_p_entities->GetWells();

	for (auto &well : wells)
	{
		if (well->m_isOpen)
		{
			float dist = glm::distance(a_p_ownerAgent->m_p_agentMoveInfo->position,
				well->m_centre);
			if (dist < 100.0f)
			{
				a_p_ownerAgent->m_p_treeInfo->m_openWell = well->m_centre;
				// Set need to find evade target to true, as Blobby is no longer
				// evade pathfinding.
				a_p_ownerAgent->m_p_treeInfo->m_newEvadeTarget = true;
				// Set wall following to false.
				a_p_ownerAgent->m_p_treeInfo->m_alongWall = false;
				return Status::BH_SUCCESS;
			}
		}
	}
	return Status::BH_FAILURE;
}

/*=============================================================================
						FindSlimeBehaviour Class.
=============================================================================*/

// Finds the closest black slime. "Closest" is defined as the Euclidean
// distance.
Status FindSlimeBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Get the squared distance between Blobby and the slimes and see which
	// is the closest.
	float minDist = INFINITY;
	std::vector<BlackSlime*> slimes = a_p_entities->GetSlimes();
	for (auto &slime : slimes)
	{
		float dist = glm::dot(a_p_ownerAgent->m_p_agentMoveInfo->position,
			slime->m_p_agentMoveInfo->position);

		if (dist < minDist)
		{
			a_p_ownerAgent->m_p_treeInfo->m_slimeVector = (
				slime->m_p_agentMoveInfo->position);
			minDist = dist;
		}
	}
	if (minDist < INFINITY)
		return Status::BH_SUCCESS;
	else
		return Status::BH_FAILURE;
}

/*=============================================================================
						SlimeToWellBehaviour Class.
=============================================================================*/

// Calculates the vector from the slime to the well. This info is used to
// find the spot for Blobby to get to to lure the slime in.
Status SlimeToWellBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Calculate the vector.
	a_p_ownerAgent->m_p_treeInfo->m_slimeToWellVector = (
		a_p_ownerAgent->m_p_treeInfo->m_openWell -
		a_p_ownerAgent->m_p_treeInfo->m_slimeVector);
	return Status::BH_SUCCESS;
}

/*=============================================================================
					GetLureTargetBehaviour Class.
=============================================================================*/

// Project the slime to well vector from the centre of the well. This will
// give a vector on the either side of the well from the slime for which
// the slime will follow into the well.
Status GetLureTargetBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Normalise the slime to well vector, then scale it by 100.
	glm::vec2 proj = glm::normalize(
		a_p_ownerAgent->m_p_treeInfo->m_slimeToWellVector);
	proj *= 100.0f;

	a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = (
		a_p_ownerAgent->m_p_treeInfo->m_openWell + proj);
	return Status::BH_SUCCESS;
}
