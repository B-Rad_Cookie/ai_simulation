/*-----------------------------------------------------------------------------
file:			BlackSlime.cpp
author:			Brad Cook
description:	This header file contains the definition of the BlackSlime
				class: The class which represents the Black Slime agent.
-----------------------------------------------------------------------------*/

#include "BlackSlime.h"
#include "EntityManager.h"
#include "StateMachine.h"
#include "Pursue.h"
#include "Circle.h"
#include "ObjAvoid.h"
#include <glm\geometric.hpp>

// Default Constructor
BlackSlime::BlackSlime(EntityManager *a_p_entities,
	Graph *a_p_worldGraph) : Agent()
{
	// Set the max speed to 2.5.
	m_p_agentMoveInfo->maxSpeed = 2.5f;

	// Create the state machine.
	m_p_stateMachine = new StateMachine(a_p_entities, this, a_p_worldGraph);

	// Set the bound circle
	m_p_boundCircle = new Circle(glm::vec2(0.0f, 0.0f), 12.5f);
}

// Constructor with Vector2 starting position.
BlackSlime::BlackSlime(EntityManager *a_p_entities,
	Graph *a_p_worldGraph,
	const glm::vec2 &a_position) : BlackSlime(a_p_entities, a_p_worldGraph)
{
	m_p_agentMoveInfo->position = a_position;
	m_p_boundCircle->m_centre = a_position;
}

// Destructor
BlackSlime::~BlackSlime()
{
	// Remove the statemachine, bound circle and sight.
	delete m_p_stateMachine;
	delete m_p_boundCircle;
}

// Update the BlackSlime information.
void BlackSlime::Update(EntityManager *a_p_entities,
						Graph *a_p_worldGraph,
						float deltaTime)
{
	// Get the collision obstacle avoidance.
	glm::vec2 avoidanceForce = (ObjAvoid::CalculateForce(
		*m_p_agentMoveInfo, a_p_entities) * deltaTime);

	// StateMachine Update
	m_p_stateMachine->Update(a_p_entities, this, a_p_worldGraph, deltaTime);

	/*
	If the obstacle avoidance returns a force that isn't vec2(0, 0), then
	weight the obstacle avoidance based on the agent's max speed.
	The faster the agent is moving, the greater the importance of the avoidance
	force. The max weight for the avoidance will be 0.1f. The absolute max
	a slime can travel is 2.0f. Therefore, to caculate the avoidance weight, it
	will be the max speed divided by 20.0f (ten times the absolute max speed).
	*/

	if (avoidanceForce != glm::vec2(0.0f, 0.0f))
	{
		float avoidWeight = m_p_agentMoveInfo->maxSpeed / 20.0f;
		float behaviourWeight = 1.0f - avoidWeight;
		m_p_agentMoveInfo->acceleration *= behaviourWeight;
		m_p_agentMoveInfo->acceleration += avoidanceForce * avoidWeight;
	}

	// Update the agent information
	// Update the velocity with the current acceleration
	m_p_agentMoveInfo->velocity += m_p_agentMoveInfo->acceleration;

	// constrain the velocity
	if (m_p_agentMoveInfo->velocity.x > m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.x = m_p_agentMoveInfo->maxSpeed;
	}
	else if (m_p_agentMoveInfo->velocity.x < -m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.x = -m_p_agentMoveInfo->maxSpeed;
	}
	if (m_p_agentMoveInfo->velocity.y > m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.y = m_p_agentMoveInfo->maxSpeed;
	}
	else if (m_p_agentMoveInfo->velocity.y < -m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.y = -m_p_agentMoveInfo->maxSpeed;
	}

	// reset the acceleration for the next frame
	m_p_agentMoveInfo->acceleration = glm::vec2(0.0f, 0.0f);

	// add the velocity to the position
	m_p_agentMoveInfo->position += m_p_agentMoveInfo->velocity;
	m_p_boundCircle->m_centre += m_p_agentMoveInfo->velocity;

	// update the agent look ahead
	m_p_agentMoveInfo->ahead = (
		m_p_agentMoveInfo->position + 
		((glm::normalize(m_p_agentMoveInfo->velocity) * 50.0f)));

	// update the line of sight.
	m_p_sight->m_centre = (
		m_p_agentMoveInfo->position +
		((glm::normalize(m_p_agentMoveInfo->velocity) * 75.0f)));
}
