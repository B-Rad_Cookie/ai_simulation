#include "Game1.h"
#include "SpriteBatch.h"
#include "Texture.h"
#include "EntityManager.h"
#include "Polygon.h"
#include "Circle.h"
#include "ColDetect.h"
#include <vector>
#include <glm\vec2.hpp>

Game1::Game1(unsigned int windowWidth, unsigned int windowHeight, bool fullscreen, const char *title) : Application(windowWidth, windowHeight, fullscreen, title)
{
	m_spritebatch = SpriteBatch::Factory::Create(this, SpriteBatch::GL3);

	// Create the Entity Manager, which will create the respective entities.
	m_p_entityManager = new EntityManager();

	// Create the Background texture;
	m_p_backgroundTexture = new Texture("./Images/Background.png");

	this->BuildGraphGrid();

	m_p_entityManager->InitAgents(m_p_graph);
}

Game1::~Game1()
{
	SpriteBatch::Factory::Destroy(m_spritebatch);

	// Delete the info created here.
	delete m_p_entityManager;
	delete m_p_backgroundTexture;
	m_p_graph->Destroy();
}


void Game1::Update(float deltaTime)
{
	// Update all the entities.
	m_p_entityManager->Update(m_p_graph, deltaTime);
}

void Game1::Draw()
{
	// clear the back buffer
	ClearScreen();

	m_spritebatch->Begin();

	m_spritebatch->DrawSprite(m_p_backgroundTexture, 320.0f, 240.0f);

	// Draw the entities
	m_p_entityManager->Draw(m_spritebatch);

	m_spritebatch->End();

}

// Creates the grid to be used for pathfinding purposes.
void Game1::BuildGraphGrid()
{
	// Initialise the graph. It will be a non-directed graph.
	m_p_graph = new Graph();

	/*
	Build an 8-way grid. Create a node every set pixel distance away within
	the game screen.

	This will be done by using a loop to iterate from 1 to the SCREEN_WIDTH
	in the x plane, and 1 to SCREEN_HEIGHT in the y plane in the set pixel
	distance steps. At each step, a node will be created.
	For each node, if there is a node existing in any of the 8 directions,
	then a create an edge between them, with a weight of 1 for nodes up,
	down, left or right, and a weight of square root 2 for all diagnonal
	connections.
	The graph is going to be built from the top left, creating a node across
	that row from left to right, then moving down to the line row. Because
	of this pattern, we don't need to check all 8 directions. There won't be
	existing nodes below (either below, below and left, and below and right),
	and there won't be a node to the right. As such, we only need to look to:
	Upper-left, Up, Upper-right and left.
	A couple notes for graph building:
	1. No Node will be placed on water, wells or obstacles.
	2. For an edge which connects to a node in the rock or pavement, the cost
	   of travel will be increased (by 20 and 10 respectively).
	*/
	
	// Get the entities to check for graph building.
	const Polygon *water = m_p_entityManager->GetWater();
	const std::vector<Well*> wells = m_p_entityManager->GetWells();
	const std::vector<Circle*> obstacles = m_p_entityManager->GetObstacles();
	const std::vector<Polygon*> rockAreas = m_p_entityManager->GetRock();
	const std::vector<Polygon*> paveAreas = m_p_entityManager->GetPaved();
	
	int betweenNodeDist = 20;
	int windowWidth = this->GetWindowWidth();
	int windowHeight = this->GetWindowHeight();


	for (int y = 10; y < windowHeight; y += betweenNodeDist)
	{
		for (int x = 10; x < windowWidth; x += betweenNodeDist)
		{
			// Check if x, y position is in the water.
			if (ColDetect::AreColliding(glm::vec2((float)x, (float)y), water))
				continue;

			//  Check if intersect with obstacles.
			bool collide = false;
			for (auto &obstacle : obstacles)
			{
				if (ColDetect::AreColliding(glm::vec2((float)x, (float)y),
					obstacle))
				{
					collide = true;
					break;
				}
			}
			if (collide)
				continue;

			//  Check if intersect with wells.
			collide = false;
			for (auto &well : wells)
			{
				if (ColDetect::AreColliding(glm::vec2((float)x, (float)y),
					well))
				{
					collide = true;
					break;
				}
			}
			if (collide)
				continue;

			// Add the new node.
			Graph::Node *p_currentNode = m_p_graph->AddNode(x, y);

			// Check to see if the new node is in a rock or pavement area. If
			// so, then all connections to this node will be with the relevant
			// weight (rock multiply by 20, pavement multiply by 10).
			float weightMultiply = 1.0f;

			glm::vec2 newNodePoint = glm::vec2((float)x, (float)y);
			// Check whether the node falls within a rock area. If so,
			// set the weight multiply to 20.
			for (auto &rock : rockAreas)
			{
				if (ColDetect::AreColliding(newNodePoint, rock))
				{
					weightMultiply = 20.0f;
					break;
				}
			}
			// If the node doesn't fall in a rock area, check whether the node
			// falls within a pavement area. If so, set the weight multiply to
			// 10.
			if (weightMultiply == 1.0f)
			{
				for (auto &pave : paveAreas)
				{
					if (ColDetect::AreColliding(newNodePoint, pave))
					{
						weightMultiply = 10.0f;
						break;
					}
				}
			}

			//  Look for an existing node to the upper left.
			int lookX = x - betweenNodeDist;
			int lookY = y - betweenNodeDist;
			this->ConnectNewNode(p_currentNode, lookX, lookY, weightMultiply,
				rockAreas, paveAreas);

			// Look for above node.
			lookX += betweenNodeDist;
			this->ConnectNewNode(p_currentNode, lookX, lookY, weightMultiply,
				rockAreas, paveAreas);

			// Look for upper-right node.
			lookX += betweenNodeDist;
			this->ConnectNewNode(p_currentNode, lookX, lookY, weightMultiply,
				rockAreas, paveAreas);

			// Look for left node.
			lookX = x - betweenNodeDist;
			lookY = y;
			this->ConnectNewNode(p_currentNode, lookX, lookY, weightMultiply,
				rockAreas, paveAreas);
		}
	}
}

// Connects a potential node at the X,Y co-ordinate a_xcoord, a_ycoord to the
// node a_p_newNode in the world graph with the edge weight muplied by
// a_weightMultiply.
void Game1::ConnectNewNode(
	Graph::Node *a_p_newNode,
	int a_xcoord,
	int a_ycoord,
	float a_weightMultiply,
	const std::vector<Polygon*> &a_rockAreas,
	const std::vector<Polygon*> &a_paveAreas)
{
	// Only look if it's not off screen.
	if (a_xcoord >= 1 && a_ycoord >= 1)
	{
		// Find the target node. Only connect if there is a node.
		Graph::Node *p_Node = m_p_graph->FindNode(a_xcoord, a_ycoord);
		if (p_Node != nullptr)
		{
			// If the new node falls within a rock or pavement, the
			// a_weightMultiply will be 10 or 20. If this value is not 1, then
			// all nodes will be multiplied by this value. In these
			// instances, don't do any checks on the destination node.
			if (a_weightMultiply == 1.0f)
			{
				// Check whether the node falls within a rock area. If so,
				// set the weight multiply to 20.
				glm::vec2 lookPoint = glm::vec2((float)a_xcoord,
												(float)a_ycoord);
				
				for (auto &rock : a_rockAreas)
				{
					if (ColDetect::AreColliding(lookPoint, rock))
					{
						a_weightMultiply = 20.0f;
						break;
					}
				}

				// If the node doesn't fall in a rock area, check whether the
				// destination node falls in a pavement area. If it does, set
				// the weight multiply to 10.
				if (a_weightMultiply == 1.0f)
				{
					for (auto &pave : a_paveAreas)
					{
						if (ColDetect::AreColliding(lookPoint, pave))
						{
							a_weightMultiply = 10.0f;
							break;
						}
					}
				}
			}

			// If the destination node is 4-way adjacent (left, right, up,
			// down) to the new node, then the standard weight is 1.0f. It it
			// is diagonal, the standard weight is 1.414214f (sqrt(1)).
			if (a_xcoord == a_p_newNode->m_x || a_ycoord == a_p_newNode->m_y)
				m_p_graph->ConnectNodes(a_p_newNode, p_Node,
										1.0f * a_weightMultiply);
			else
				m_p_graph->ConnectNodes(a_p_newNode, p_Node,
										1.414214f * a_weightMultiply);
		}
	}
}
