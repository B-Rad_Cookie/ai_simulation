/*-----------------------------------------------------------------------------
file:			TargetBehaviours.cpp
author:			Brad Cook
description:	This cpp file contains the definitions of classes (nodes)
used to perform setting pathfinding targets behaviours as part
of a Behaviour Tree.
-----------------------------------------------------------------------------*/

#include "TargetBehaviours.h"
#include "Blobby.h"
#include "BlackSlime.h"
#include "EntityManager.h"
#include "Polygon.h"
#include "Circle.h"
#include "ColDetect.h"
#include <glm\geometric.hpp>
#include <vector>

/*=============================================================================
					CrateAvailableBehaviour Class.
=============================================================================*/

// Determines whether a crate is available. Returns success if it is,
// returns failure otherwise.
Status CrateAvailableBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	const Polygon *p_crate = a_p_entities->GetCrate();

	if (p_crate == nullptr)
	{
		return Status::BH_FAILURE;
	}
	else
	{
		// Set the evade path target to true, as Blobby is no longer evade
		// pathfinding.
		a_p_ownerAgent->m_p_treeInfo->m_newEvadeTarget = true;
		// Set the wall following to false
		a_p_ownerAgent->m_p_treeInfo->m_alongWall = false;
		return  Status::BH_SUCCESS;
	}
}

/*=============================================================================
						TargetCrateBehaviour Class.
=============================================================================*/

// Sets the crate as the target
Status TargetCrateBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	const Polygon *p_crate = a_p_entities->GetCrate();

	a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = p_crate->m_centre;
	return Status::BH_SUCCESS;
}

/*=============================================================================
						IsEvadeBehaviour Class.
=============================================================================*/

// Evading is determined by whether the max speed is set to the max of 2.5f.
// Returns success if she is, returns failure otherwise.
Status IsEvadeBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	if (a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed == 2.5f)
	{
		return Status::BH_SUCCESS;
	}
	else
	{
		return Status::BH_FAILURE;
	}
}

/*=============================================================================
						FollowWallBehaviour Class.
=============================================================================*/

// Checks whether Blobby is within a distance of any of the walls. Return
// Success if it is, return Failure if it isn't following the wall or not
// within a distance.
Status FollowWallBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	if (a_p_ownerAgent->m_p_treeInfo->m_alongWall == false)
		return Status::BH_FAILURE;

	std::vector<Polygon*> walls = a_p_entities->GetWalls();
	glm::vec2 blob = a_p_ownerAgent->m_p_agentMoveInfo->position;

	for (auto &wall : walls)
	{
		glm::vec2 wallVertex = wall->m_indices[0];
		// Check whether Blobby's position is within 150 pixels of a wall.
		if ((blob.x < wallVertex.x + 150.0f || blob.x > wallVertex.x - 150.0f) ||
			(blob.y < wallVertex.y + 150.0f || blob.y > wallVertex.y - 150.0f))
		{
			return Status::BH_SUCCESS;
		}
	}

	// Return Failure when not within walls.
	return Status::BH_FAILURE;
}

/*=============================================================================
						AimAtWallBehaviour Class.
=============================================================================*/

// Checks whether Blobby, while evading, has a wall ahead of her.
// Returns success if she does, returns failure otherwise. Stores the wall that
// is ahead as well.
Status AimAtWallBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	std::vector<Polygon*> walls = a_p_entities->GetWalls();

	for (auto &wall : walls)
	{
		// Check whether the ahead vector is colliding with the wall.
		if (ColDetect::AreColliding(a_p_ownerAgent->m_p_agentMoveInfo->position,
			a_p_ownerAgent->m_p_sight->m_centre,
			wall))
		{
			// Set the need to calculate a new evade target to true, as Blobby is not
			// evade-targeting.
			a_p_ownerAgent->m_p_treeInfo->m_newEvadeTarget = true;
			a_p_ownerAgent->m_p_treeInfo->m_alongWall = true;
			a_p_ownerAgent->m_p_treeInfo->m_p_aheadWall = wall;
			return Status::BH_SUCCESS;
		}
	}

	// Return Failure when no wall collide.
	return Status::BH_FAILURE;
}

/*=============================================================================
						TargetEndWallBehaviour Class.
=============================================================================*/

// Determines which end of the wall Blobby should target. It does this by
// chosing the wall end that is the same side of the line from Blobby to the
// wall that Blobby is from the same line.
Status TargetEndWallBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// To get the ends of the wall, use two vertices that are in the opposite
	// corners of each other. As all polygons have been defined with each
	// vertex in a clockwise-direction, this means the 1st and 3rd vertext will
	// always be opposite each other.

	glm::vec2 endOne = (
		a_p_ownerAgent->m_p_treeInfo->m_p_aheadWall->m_indices[0]);
	glm::vec2 endTwo = (
		a_p_ownerAgent->m_p_treeInfo->m_p_aheadWall->m_indices[2]);

	// Get the wall segment line and wall start to slime vector.
	glm::vec2 wallLineSegment = endTwo - endOne;
	glm::vec2 segStartToSlime = (
		a_p_ownerAgent->m_p_treeInfo->m_slimeVector - endOne);

	// Project the wall start to slime vector onto the wall segment vector by
	// getting the dot product of the segStartToSlime and normalised seg
	// vector.
	glm::vec2 unitSegVector = glm::normalize(wallLineSegment);
	float projectedDistance = glm::dot(segStartToSlime, unitSegVector);

	// Before scaling the unit seg vector to get the closest point, we need to
	// deal with two special cases. There is a possibility that the closest
	// point of the segment to the slime is one of the end points of the
	// segment. This can be determined by determining the projected distance.
	// If the projected distance is less than 0, the seg start is the closest
	// point: if it is greater than the magnitude of the segment vector, the the
	//segment end is the closest point. Otherwise, the closest point is found
	// by finding the vector which is scaled by the projectedDistance on the
	// seg unit vector, then adding this vector to the segment start.
	glm::vec2 closestPoint;
	if (projectedDistance < 0.0f)
	{
		closestPoint = endOne;
	}
	else if (projectedDistance > glm::length(wallLineSegment))
	{
		closestPoint = endTwo;
	}
	else
	{
		glm::vec2 closestVector = projectedDistance * unitSegVector;
		closestPoint = endOne + closestVector;
	}

	// Now, determine what side of the line Blobby and the wall point ends are
	// in relation to the slime to wall line segment. Use the sign of the
	// determinant of vectors to do this.
	float blobResult = (
		(closestPoint.x - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.x) *
		(a_p_ownerAgent->m_p_agentMoveInfo->position.y -
			a_p_ownerAgent->m_p_treeInfo->m_slimeVector.y) -
		(closestPoint.y - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.y) *
		(a_p_ownerAgent->m_p_agentMoveInfo->position.x -
			a_p_ownerAgent->m_p_treeInfo->m_slimeVector.x));
	float endOneResult = (
		(closestPoint.x - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.x) *
		(endOne.y - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.y) -
		(closestPoint.y - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.y) *
		(endOne.x - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.x));
	float endTwoResult = (
		(closestPoint.x - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.x) *
		(endTwo.y - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.y) -
		(closestPoint.y - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.y) *
		(endTwo.x - a_p_ownerAgent->m_p_treeInfo->m_slimeVector.x));

	// Pick the side which has the same sign (positive, negative) as blobby.
	if (blobResult > 0)
	{
		if (endOneResult > 0)
		{
			a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = endOne;
		}
		else
		{
			a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = endTwo;
		}
	}
	else
	{
		if (endOneResult < 0)
		{
			a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = endOne;
		}
		else
		{
			a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = endTwo;
		}
	}

	// Move the targets in a little from the wall. Use the Screen Width and
	// Height (640x 480) to determine this.
	if (a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.x < 10.0f)
	{
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.x += 30.0f;
	}
	else if (a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.x > 630.0f)
	{
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.x -= 30.0f;
	}

	if (a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.y < 10.0f)
	{
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.y += 30.0f;
	}
	else if (a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.y > 470.0f)
	{
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.y -= 30.0f;
	}

	return Status::BH_SUCCESS;
}

/*=============================================================================
						NewEvadeTargetBehaviour Class.
=============================================================================*/

// Determines whether a new target away from slimes needs to be set.
// Returns success if it does, returns failure otherwise.
Status NewEvadeTargetBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// If Blobby has reached the end, a new target will need to be set.
	if (a_p_ownerAgent->m_p_treeInfo->m_pathIndex ==
		a_p_ownerAgent->m_p_treeInfo->m_shortestPath.size() - 1)
	{
		return Status::BH_SUCCESS;
	}

	if (a_p_ownerAgent->m_p_treeInfo->m_newEvadeTarget)
	{
		return Status::BH_SUCCESS;
	}
	else
	{
		return Status::BH_FAILURE;
	}
}

/*=============================================================================
					SetEvadeTargetBehaviour Class.
=============================================================================*/

// Sets the pathfinding target as 200 pixels away from all of the
// black slimes.
Status SetEvadeTargetBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Add all the normalised vectors from the slime to Blobby together.
	glm::vec2 allSlimes = glm::vec2(0.0f, 0.0f);
	std::vector<BlackSlime*> slimes = a_p_entities->GetSlimes();

	for (auto &slime : slimes)
	{
		glm::vec2 toBlob = (a_p_ownerAgent->m_p_agentMoveInfo->position -
							slime->m_p_agentMoveInfo->position);
		toBlob = glm::normalize(toBlob);
		allSlimes += toBlob;
	}

	// Now normalise the vector and scale it by 200. Add that to Blobby, and
	// we have set the evade target.
	allSlimes = glm::normalize(allSlimes);
	allSlimes = allSlimes * 200.0f;
	a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = (
		a_p_ownerAgent->m_p_agentMoveInfo->position + allSlimes);

	// Set the evade target to false as Blobby is evade pathfinding.
	a_p_ownerAgent->m_p_treeInfo->m_newEvadeTarget = false;
	// Set the wall following to false
	a_p_ownerAgent->m_p_treeInfo->m_alongWall = false;

	return Status::BH_SUCCESS;
}

/*=============================================================================
						SetLastCrateBehaviour Class.
=============================================================================*/

// Sets the pathfinding target as 150 pixels in the direction of all the
// black slimes.
Status SetLastCrateBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Set the evade target to true as Blobby is no longer evade pathfinding.
	a_p_ownerAgent->m_p_treeInfo->m_newEvadeTarget = true;
	// Set the wall following to false
	a_p_ownerAgent->m_p_treeInfo->m_alongWall = false;

	// Set the last known crate as the target.
	a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = (
		a_p_entities->GetLastCrateSpawnPoint());

	return Status::BH_SUCCESS;
}
