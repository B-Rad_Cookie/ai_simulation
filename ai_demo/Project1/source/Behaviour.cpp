/*-----------------------------------------------------------------------------
file:			Behaviour.cpp
author:			Brad Cook
description:	This cpp file contains the definition of the Behaviour
				class: The base class for all behaviours of a behaviour tree.
				Source: http://www.aigamedev.com
-----------------------------------------------------------------------------*/

#include "Behaviour.h"
#include "EntityManager.h"
#include "Blobby.h"
#include "Graph.h"


Status Behaviour::tick(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
    if (m_eStatus != BH_RUNNING)
    {
        onInitialize();
    }

	m_eStatus = update(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph,
		deltaTime);

    if (m_eStatus != BH_RUNNING)
    {
        onTerminate(m_eStatus);
    }
    return m_eStatus;
}

void Behaviour::reset()
{
    m_eStatus = BH_INVALID;
}

void Behaviour::abort()
{
    onTerminate(BH_ABORTED);
    m_eStatus = BH_ABORTED;
}
	
bool Behaviour::isTerminated() const
{
    return m_eStatus == BH_SUCCESS  ||  m_eStatus == BH_FAILURE;
}

bool Behaviour::isRunning() const
{
	return m_eStatus == BH_RUNNING;
}

Status Behaviour::getStatus() const
{
    return m_eStatus;
}