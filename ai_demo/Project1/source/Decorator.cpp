/*-----------------------------------------------------------------------------
file:			Decorator.cpp
author:			Brad Cook
description:	This cpp file contains the definition of the Decorator
				class: The base class for decorator node behaviours of a
				behaviour tree.
-----------------------------------------------------------------------------*/

#include "Decorator.h"

Decorator::Decorator()
{
	m_p_child = nullptr;
}

Decorator::~Decorator()
{
	removeChild();
}

// Adds a Behvaiour as the child. Overrides any existing behaviour.
void Decorator::addChild(Behaviour* child)
{
	// Check if it is already set.
	if (m_p_child != nullptr)  // Delete first.
	{
		removeChild();
	}

	m_p_child = child;
}

void Decorator::removeChild()
{
	delete m_p_child;
	m_p_child = nullptr;
}
