/*-----------------------------------------------------------------------------
file:			Composite.cpp
author:			Brad Cook
description:	This cpp file contains the definition of the Composite
				class: The base class for composite (collections of) behaviours
				of a behaviour tree.
				Source: http://www.aigamedev.com
-----------------------------------------------------------------------------*/

#include "Composite.h"
#include <algorithm>

Composite::~Composite()
{
	clearChildren();
}

void Composite::addChild(Behaviour* child)
{
	m_children.push_back(child);
}

void Composite::removeChild(Behaviour* child)
{
	m_children.erase(std::remove(m_children.begin(), m_children.end(), child), m_children.end());
	delete child;
}

void Composite::clearChildren()
{
	for(Behaviours::iterator it = m_children.begin(); it != m_children.end(); it++) {
		delete *it;
	}
	m_children.clear();
}
