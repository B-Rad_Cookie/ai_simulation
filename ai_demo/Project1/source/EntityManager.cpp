/*-----------------------------------------------------------------------------
file:			EntityManager.cpp
author:			Brad Cook
description:	This header file contains the definition of the EntityManager
				class: The class which contains and controls all game objects.
-----------------------------------------------------------------------------*/

#include "EntityManager.h"
#include "Texture.h"
#include "Blobby.h"
#include "BlackSlime.h"
#include "Polygon.h"
#include "Circle.h"
#include "ColDetect.h"
#include "SpriteBatch.h"
#include "Timer.h"
#include <glm\vec2.hpp>
#include <time.h>
#include <random>
#include <algorithm>


// Default Constructor
EntityManager::EntityManager()
{
	srand(time(NULL));
	Init();
}

// Destructor
EntityManager::~EntityManager()
{
	DestroyAgents();
	Destroy();
}

// The initial position for all the game world entities.
void EntityManager::Init()
{
	// Set the textures.
	m_p_blobbyT = new Texture("./Images/blobby.png");
	m_p_slimeT = new Texture("./Images/jelly.png");
	m_p_openWellT = new Texture("./Images/well.png");
	m_p_closedWellT = new Texture("./Images/lid.png");
	m_p_obstacleT = new Texture("./Images/obstacle.png");
	m_p_rockT = new Texture("./Images/rock_tile.png");
	m_p_paveT = new Texture("./Images/pave_tile.png");
	m_p_vertWallsT = new Texture("./Images/SideWall.png");
	m_p_horizWallsT = new Texture("./Images/HorizontalWall.png");
	m_p_waterT = new Texture("./Images/water.png");
	m_p_sightT = new Texture("./Images/lsight.png");
	m_p_crateT = new Texture("./Images/crate.png");

	// Set the crate to null ptr
	m_p_crate = nullptr;

	// Create the walls.
	m_sidewalls = {  // Left Wall
		new Polygon(std::vector<glm::vec2>{glm::vec2(0.0f, 0.0f),
										   glm::vec2(5.0f, 0.0f),
										   glm::vec2(5.0f, 480.0f),
										   glm::vec2(0.0f, 480.0f)},
					glm::vec2(2.5f, 240.0f)),
		// Right Wall
		new Polygon(std::vector<glm::vec2>{glm::vec2(635.0f, 0.0f),
										   glm::vec2(640.0f, 0.0f),
										   glm::vec2(640.0f, 480.0f),
										   glm::vec2(635.0f, 480.0f)},
					glm::vec2(637.5f, 240.0f)),
		// Top Wall
		new Polygon(std::vector<glm::vec2>{glm::vec2(0.0f, 0.0f),
										   glm::vec2(640.0f, 0.0f),
										   glm::vec2(640.0f, 5.0f),
										   glm::vec2(0.0f, 5.0f)},
					glm::vec2(320.0f, 2.5f)),
		// Bottom Wall
		new Polygon(std::vector<glm::vec2>{glm::vec2(0.0f, 475.0f),
										   glm::vec2(640.0f, 475.0f),
										   glm::vec2(640.0f, 480.0f),
										   glm::vec2(0.0f, 480.0f)},
					glm::vec2(320.0f, 477.5f))
	};

	// Create the obstacles
	m_obstacles = {
		new Circle(glm::vec2(170.0f, 70.0f), 22.0f),
		new Circle(glm::vec2(350.0f, 90.0f), 22.0f),
		new Circle(glm::vec2(510.0f, 150.0f), 22.0f),
		new Circle(glm::vec2(90.0f, 270.0f), 22.0f),
		new Circle(glm::vec2(470.0f, 390.0f), 22.0f),
	};

	// Create the water
	m_p_water = new Polygon(std::vector<glm::vec2>{glm::vec2(245.0f, 165.0f),
												   glm::vec2(395.0f, 165.0f),
												   glm::vec2(395.0f, 315.0f),
												   glm::vec2(245.0f, 315.0f)},
							glm::vec2(320.0f, 240.0f));

	// Create the wells
	m_wells = {
		new Well(glm::vec2(120.0f, 120.0f), 15.0f, true),
		new Well(glm::vec2(510.0f, 270.0f), 15.0f, true),
		new Well(glm::vec2(120.0f, 400.0f), 15.0f, true)
	};

	// Create the rocky areas.
	m_rockAreas = { 
		new Polygon(std::vector<glm::vec2>{glm::vec2(320.0f, 60.0f),
										   glm::vec2(380.0f, 60.0f),
										   glm::vec2(380.0f, 120.0f),
										   glm::vec2(320.0f, 120.0f)},
					glm::vec2(350.0f, 90.0f)),
		new Polygon(std::vector<glm::vec2>{glm::vec2(420.0f, 180.0f),
										   glm::vec2(480.0f, 180.0f),
										   glm::vec2(480.0f, 240.0f),
										   glm::vec2(420.0f, 240.0f)},
					glm::vec2(450.0f, 210.0f)),
		new Polygon(std::vector<glm::vec2>{glm::vec2(260.0f, 340.0f),
										   glm::vec2(320.0f, 340.0f),
										   glm::vec2(320.0f, 400.0f),
										   glm::vec2(260.0f, 400.0f)},
					glm::vec2(290.0f, 370.0f)),
		new Polygon(std::vector<glm::vec2>{glm::vec2(40.0f, 320.0f),
										   glm::vec2(100.0f, 320.0f),
										   glm::vec2(100.0f, 380.0f),
										   glm::vec2(40.0f, 380.0f)},
					glm::vec2(70.0f, 350.0f))
	};

	// Create the paved areas.
	m_pavedAreas = {
		new Polygon(std::vector<glm::vec2>{glm::vec2(60.0f, 160.0f),
										   glm::vec2(120.0f, 160.0f),
										   glm::vec2(120.0f, 220.0f),
										   glm::vec2(60.0f, 220.0f)},
					glm::vec2(90.0f, 190.0f)),
		new Polygon(std::vector<glm::vec2>{glm::vec2(180.0f, 160.0f),
										   glm::vec2(240.0f, 160.0f),
										   glm::vec2(240.0f, 220.0f),
										   glm::vec2(180.0f, 220.0f)},
					glm::vec2(210.0f, 190.0f)),
		new Polygon(std::vector<glm::vec2>{glm::vec2(480.0f, 180.0f),
										   glm::vec2(540.0f, 180.0f),
										   glm::vec2(540.0f, 240.0f),
										   glm::vec2(480.0f, 240.0f)},
					glm::vec2(510.0f, 210.0f)),
		new Polygon(std::vector<glm::vec2>{glm::vec2(420.0f, 300.0f),
										   glm::vec2(480.0f, 300.0f),
										   glm::vec2(480.0f, 360.0f),
										   glm::vec2(420.0f, 360.0f)},
					glm::vec2(450.0f, 330.0f))
	};

	// Set the respawn timers.
	m_p_slimeRespawnTimer = new Timer(60.0f);
	m_p_crateRespawnTimer = new Timer(50.0f);

	// Set the crate spawn spots.
	m_crateSpawnSpots = {
		glm::vec2(540.0f, 430.0f),
		glm::vec2(480.0f, 40.0f),
		glm::vec2(60.0f, 150.0f),
		glm::vec2(240.0f, 420.0f)
	};
	m_crateSpawnIndex = 3;
}

// Create the Game agents.
void EntityManager::InitAgents(Graph *a_p_worldGraph)
{
	glm::vec2 blobbySpawn[2] = {
		glm::vec2(480.0f, 30.0f),
		glm::vec2(340.0f, 400.0f)
	};
	int spawn = rand() % 2;
	// Create Blobby
	m_p_blobby = new Blobby(blobbySpawn[spawn]);

	// Create the black slime.
	CreateBlackSlime(a_p_worldGraph);
}

// Removes all the game world components.
void EntityManager::Destroy()
{
	// Destroy the textures
	delete m_p_blobbyT;
	delete m_p_slimeT;
	delete m_p_openWellT;
	delete m_p_closedWellT;
	delete m_p_obstacleT;
	delete m_p_rockT;
	delete m_p_paveT;
	delete m_p_vertWallsT;
	delete m_p_horizWallsT;
	delete m_p_waterT;
	delete m_p_sightT;
	delete m_p_crateT;

	// Destroy the obstacles
	for (auto &obstacle : m_obstacles)
	{
		delete obstacle;
	}
	m_obstacles.clear();

	// Destroy the wells.
	for (auto &well : m_wells)
	{
		delete well;
	}
	m_wells.clear();

	// Destroy the rocky areas
	for (auto &rock : m_rockAreas)
	{
		delete rock;
	}
	m_rockAreas.clear();

	// Destroy the Paved areas
	for (auto &pave : m_pavedAreas)
	{
		delete pave;
	}
	m_pavedAreas.clear();

	// Destroy the water
	delete m_p_water;

	// Destroy the crate (if there is one)
	if (m_p_crate != nullptr)
	{
		delete m_p_crate;
	}

	// Destroy the Walls
	for (auto &wall : m_sidewalls)
	{
		delete wall;
	}
	m_sidewalls.clear();

	// Destroy timers.
	delete m_p_slimeRespawnTimer;
	delete m_p_crateRespawnTimer;

	// Clear crate spawn spots
	m_crateSpawnSpots.clear();
}

// Removes the game world agents.
void EntityManager::DestroyAgents()
{
	// Destroy Blobby
	delete m_p_blobby;

	// Destroy the Black Slimes
	for (auto &slime : m_slimes)
	{
		delete slime;
	}
	m_slimes.clear();
}

// Updates all the required entities.
void EntityManager::Update(Graph *a_p_worldGraph, float dT)
{
	// Update Blobby
	m_p_blobby->Update(this, a_p_worldGraph, dT);

	// Update all the black slime info.
	for (auto &slime : m_slimes)
	{
		slime->Update(this, a_p_worldGraph, dT);
	}

	// Check whether any Black slimes have been destroyed.
	BlackSlime* p_slimeToRemove = nullptr;

	for (auto &slime : m_slimes)
	{
		if (m_p_blobby->IsPoweredUp())
		{
			if (ColDetect::AreColliding(slime->m_p_boundCircle,
				m_p_blobby->m_p_boundCircle))
			{
				p_slimeToRemove = slime;
				break;
			}
		}
		for (auto &well : m_wells)
		{
			if (well->m_isOpen)
			{
				if (ColDetect::AreColliding(slime->m_p_boundCircle,
					well))
				{
					p_slimeToRemove = slime;
					well->m_isOpen = false;
					break;
				}
			}
		}
	}
	if (p_slimeToRemove != nullptr)
	{
		RemoveSlime(p_slimeToRemove);
	}

	// Check whether a Black Slime needs to be created.
	if (m_slimes.size() < 2)
	{
		if (m_p_slimeRespawnTimer->TimeElapsed() ||
			m_slimes.size() == 0)
			CreateBlackSlime(a_p_worldGraph);
		else
			m_p_slimeRespawnTimer->AddTime(dT);
	}

	// Check whether a crate needs to be spawned.
	if (m_p_crate == nullptr)
	{
		if (m_p_crateRespawnTimer->TimeElapsed())
			CreateCrate();
		else
			m_p_crateRespawnTimer->AddTime(dT);
	}

	// Check whether wells need to be re-opened.
	for (auto &well : m_wells)
	{
		if (!well->m_isOpen)
		{
			if (well->TimeElapsed())
			{
				well->m_isOpen = true;
				well->Reset();
			}
			else
			{
				well->AddTime(dT);
			}
		}
	}

	// Check whether Blobby has been defeated.  If so, reset Blobby and the
	// Slimes.
	bool restart = false;
	if (!m_p_blobby->IsPoweredUp())
	{
		for (auto &slime : m_slimes)
		{
			if (ColDetect::AreColliding(m_p_blobby->m_p_boundCircle,
				slime->m_p_boundCircle))
			{
				restart = true;
				break;
			}
		}
	}
	if (restart)
	{
		DestroyAgents();
		InitAgents(a_p_worldGraph);
	}
}

// Draws all the entities.
void EntityManager::Draw(SpriteBatch *a_p_spritebatch)
{
	// Draw the line of sights.
	for (auto &slime : m_slimes)
	{
		a_p_spritebatch->DrawSprite(m_p_sightT,
			slime->m_p_sight->m_centre.x,
			slime->m_p_sight->m_centre.y);
	}

	a_p_spritebatch->DrawSprite(m_p_sightT,
		m_p_blobby->m_p_sight->m_centre.x,
		m_p_blobby->m_p_sight->m_centre.y);

	// Draw the walls. Left, Right, top, then bottom.
	for (int i = 0; i < m_sidewalls.size(); i++)
	{
		// Draw vertical for first 2, horizontal for 2nd two.
		if (i < 2)
		{
			a_p_spritebatch->DrawSprite(m_p_vertWallsT,
				m_sidewalls.at(i)->m_centre.x,
				m_sidewalls.at(i)->m_centre.y);
		}
		else
		{
			a_p_spritebatch->DrawSprite(m_p_horizWallsT,
				m_sidewalls.at(i)->m_centre.x,
				m_sidewalls.at(i)->m_centre.y);
		}
	}

	// Draw the water
	a_p_spritebatch->DrawSprite(m_p_waterT,
		m_p_water->m_centre.x,
		m_p_water->m_centre.y);

	// Draw the rocky areas.
	for (auto &rock : m_rockAreas)
	{
		a_p_spritebatch->DrawSprite(m_p_rockT,
			rock->m_centre.x,
			rock->m_centre.y);
	}

	// Draw the paved areas.
	for (auto &pave : m_pavedAreas)
	{
		a_p_spritebatch->DrawSprite(m_p_paveT,
			pave->m_centre.x,
			pave->m_centre.y);
	}
	
	// Draw all the obstacles.
	for (auto &obstacle : m_obstacles)
	{
		a_p_spritebatch->DrawSprite(m_p_obstacleT,
			obstacle->m_centre.x,
			obstacle->m_centre.y);
	}

	// Draw all the wells.
	for (auto &well : m_wells)
	{
		if (well->m_isOpen)
			a_p_spritebatch->DrawSprite(m_p_openWellT,
				well->m_centre.x,
				well->m_centre.y);
		else
			a_p_spritebatch->DrawSprite(m_p_closedWellT,
				well->m_centre.x,
				well->m_centre.y);
	}

	// Draw the crate (if there is one).
	if (m_p_crate != nullptr)
	{
		a_p_spritebatch->DrawSprite(m_p_crateT,
			m_p_crate->m_centre.x,
			m_p_crate->m_centre.y);
	}

	// Draw all the black slimes.
	for (auto &slime : m_slimes)
	{
		a_p_spritebatch->DrawSprite(m_p_slimeT,
			slime->m_p_agentMoveInfo->position.x,
			slime->m_p_agentMoveInfo->position.y);
	}

	// Draw Blobby
	if (m_p_blobby->IsPoweredUp())
	{ 
		a_p_spritebatch->SetRenderColor(255, 0, 0, 255);
	}
	a_p_spritebatch->DrawSprite(m_p_blobbyT,
		m_p_blobby->m_p_agentMoveInfo->position.x,
		m_p_blobby->m_p_agentMoveInfo->position.y);
}

// Creates a Black Slime at one of the wells.
void EntityManager::CreateBlackSlime(Graph* a_p_worldGraph)
{
	// Create a black slime at a well and reset the timer.

	// Pick a random well. Close that well.
	int well = rand() % m_wells.size();
	m_wells[well]->m_isOpen = false;
	m_wells[well]->Reset();

	BlackSlime *p_slime = new BlackSlime(this, a_p_worldGraph,
		m_wells[well]->m_centre);
	m_slimes.push_back(p_slime);

	m_p_slimeRespawnTimer->Reset();
}

// Creates a crate at a random location.
void EntityManager::CreateCrate()
{
	// Create a crate at one of the locations and reset the timer.

	// Pick a location.
	m_crateSpawnIndex = rand() % m_crateSpawnSpots.size();
	glm::vec2 resSpot = m_crateSpawnSpots[m_crateSpawnIndex];

	// With the crate, the indices must be 20 pixels either side of the centre
	// point (20 pixels is half the size of the crate sprite).
	m_p_crate = new Polygon(std::vector<glm::vec2>{
		// Top Left
		glm::vec2(resSpot.x - 20.0f, resSpot.y - 20.0f),
		// Top Right
		glm::vec2(resSpot.x + 20.0f, resSpot.y - 20.0f),
		// Bottom Right
		glm::vec2(resSpot.x + 20.0f, resSpot.y + 20.0f),
		// Bottom Left
		glm::vec2(resSpot.x - 20.0f, resSpot.y + 20.0f)},
		// Centre
		resSpot);
}

// Removes the Crate from the game world.
void EntityManager::RemoveCrate()
{
	// Delete the crate, set to nullptr, an reset timer.
	delete m_p_crate;
	m_p_crate = nullptr;
	m_p_crateRespawnTimer->Reset();
}


// Removes the given Slime.
void EntityManager::RemoveSlime(BlackSlime* a_p_slime)
{
	// Remove the lsime from the std::vector, then delete the memory.
	m_slimes.erase(remove(m_slimes.begin(), m_slimes.end(), a_p_slime), m_slimes.end());

	delete a_p_slime;
}
