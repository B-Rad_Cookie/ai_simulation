/*-----------------------------------------------------------------------------
file:			Seek.cpp
author:			Brad Cook
description:	This header file contains the definition of the Seek class:
				The class which implements the Seek AI Steering behaviour.
-----------------------------------------------------------------------------*/

#include "Seek.h"
#include <glm/geometric.hpp>


// Returns the glm::vec2 force to apply to an agent such that the agent
// moves to the target agent.
glm::vec2 Seek::CalculateForce(const MovementInfo &a_agent,
							   const MovementInfo &a_target)
{
	// Do the seek.
	// Calculate a vector from the current position to the target.
	glm::vec2 toTargetVector = a_target.position - a_agent.position;

	// Normalise the vector (divide it by it's magnitude).
	toTargetVector = glm::normalize(toTargetVector);

	// factor in the Agent's max Velocity.
	toTargetVector = toTargetVector * a_agent.maxSpeed;

	// Now get the force.
	glm::vec2 seekForce = toTargetVector - a_agent.velocity;

	// Return the force to the velocity
	return seekForce;
}

// Returns the glm::vec2 force to apply to an agent such that the agent
// moves to the target vector.
glm::vec2 Seek::CalculateForce(const MovementInfo &a_agent,
							   const glm::vec2 &a_target)
{
	// Do the seek.
	// Calculate a vector from the current position to the target.
	glm::vec2 toTargetVector = a_target - a_agent.position;

	// Normalise the vector (divide it by it's magnitude).
	toTargetVector = glm::normalize(toTargetVector);

	// factor in the Agent's max Velocity.
	toTargetVector = toTargetVector * a_agent.maxSpeed;

	// Now get the force.
	glm::vec2 seekForce = toTargetVector - a_agent.velocity;

	// Return the force to the velocity
	return seekForce;
}