/*-----------------------------------------------------------------------------
file:			Selector.cpp
author:			Brad Cook
description:	This cpp file contains the definition of the Selector
				class: The class for the Selector node behaviour of a behaviour
				tree.
				Source: http://www.aigamedev.com
-----------------------------------------------------------------------------*/

#include "Selector.h"

Selector::~Selector()
{
	clearChildren();
}

void Selector::onInitialize()
{
	m_current = m_children.begin();
}

Status Selector::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
    // Keep going until a child behavior says its running.
	while(true)
    {
		Status s = (*m_current)->tick(a_p_entities,
			a_p_ownerAgent,
			a_p_worldGraph,
			deltaTime);

        // If the child succeeds, or keeps running, do the same.
        if (s != BH_FAILURE)
        {
            return s;
        }

        // Hit the end of the array, it didn't end well...
        if (++m_current == m_children.end())
        {
            return BH_FAILURE;
        }
    }
}

