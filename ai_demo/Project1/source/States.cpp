/*-----------------------------------------------------------------------------
file:			States.cpp
author:			Brad Cook
description:	This header file contains the definition of all the states
				for the black slime state machine.
-----------------------------------------------------------------------------*/

#include "States.h"
#include "EntityManager.h"
#include "Agent.h"
#include "Blobby.h"
#include "Seek.h"
#include "Pursue.h"
#include "Evade.h"
#include "ColDetect.h"
#include "Polygon.h"
#include "Circle.h"
#include "AStar.h"
#include <glm\geometric.hpp>

/*============================================================================
							BaseFindState Class
=============================================================================*/

// Adds the force to a black slime which is required to get to Blobby. This
// done by finding the shortest path to Blobby (using the A* algorithm),
// then following the path to Blobby. The a_changeState is used to indicate
// back to the StateMachine what state the state machine should switch to.
void BaseFindState::Update(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	SlimeState &a_changeState,
	float deltaTime)
{
	// Check if Target or Slime has moved such that they have moved closer to
	// another node.
	Graph::Node* p_currentTarg = a_p_worldGraph->FindNearestNode(
		m_targetVector.x, m_targetVector.y);
	Graph::Node* p_currentSlime = a_p_worldGraph->FindNearestNode(
		a_p_ownerAgent->m_p_agentMoveInfo->position.x,
		a_p_ownerAgent->m_p_agentMoveInfo->position.y);

	// If there is no path or the target has reached the end of the path,
	// recalculate
	if (m_shortestPath.empty() ||
		m_currentNodeIndex >= m_shortestPath.size() - 1)
	{
		SetShortestPath(a_p_entities, a_p_ownerAgent, a_p_worldGraph);
	}
	// If a Node has changed, we need to re-calculate the shortest path.
	else if (*p_currentTarg != *m_p_target ||
		*p_currentSlime != *m_shortestPath[0])
	{
		SetShortestPath(a_p_entities, a_p_ownerAgent, a_p_worldGraph);
	}

	// When there is no shortest path (i.e. the slime and Blobby are on the
	// same node), or the slime is at the end of the path, seek the target.
	if (m_shortestPath.empty() ||
		m_currentNodeIndex >= m_shortestPath.size() - 1)
	{
		a_p_ownerAgent->m_p_agentMoveInfo->acceleration += (Seek::CalculateForce(
			*a_p_ownerAgent->m_p_agentMoveInfo, m_targetVector) * deltaTime);
		return;
	}

	// If the black slime has reached its current target node, update target
	// increment.
	glm::vec2 targetNodeVec = glm::vec2(
		m_shortestPath[m_currentNodeIndex]->m_x,
		m_shortestPath[m_currentNodeIndex]->m_y);

	if (ColDetect::AreColliding(targetNodeVec,
			a_p_ownerAgent->m_p_boundCircle))
	{
		m_currentNodeIndex++;
		targetNodeVec = glm::vec2(
			m_shortestPath[m_currentNodeIndex]->m_x,
			m_shortestPath[m_currentNodeIndex]->m_y);
	}

	// Add the force required to move the black slime to the target
	a_p_ownerAgent->m_p_agentMoveInfo->acceleration += (Seek::CalculateForce(
		*a_p_ownerAgent->m_p_agentMoveInfo, targetNodeVec) * deltaTime);
}

// Initialises the state. Set the shortest path to Blobby.
void BaseFindState::Init(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph)
{
	// Set the path.
	SetShortestPath(a_p_entities, a_p_ownerAgent, a_p_worldGraph);
}

// Exit function. Cleans up all the memory used.
void BaseFindState::Exit()
{
	// Clear the shortest path info.
	m_shortestPath.clear();

	// Set the target node to null
	m_p_target = nullptr;
}

// Sets the shortest path from the black slime to the target vector.
void BaseFindState::SetShortestPath(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph)
{
	// Find the node closest to the target.
	m_p_target = a_p_worldGraph->FindNearestNode(
		m_targetVector.x, m_targetVector.y);

	// Find the node closest to the Black Slime.
	Graph::Node *p_slimeNode = a_p_worldGraph->FindNearestNode(
		a_p_ownerAgent->m_p_agentMoveInfo->position.x,
		a_p_ownerAgent->m_p_agentMoveInfo->position.y);

	// Now get the shortest path.
	m_shortestPath = std::vector<Graph::Node*>();
	AStar *p_astar = new AStar();
	p_astar->AStarShortestPath(*a_p_worldGraph,
		p_slimeNode, m_p_target, m_shortestPath);

	// Set the target index to the first node.
	m_currentNodeIndex = 0;
}

/*============================================================================
							FindState Class
=============================================================================*/

// Adds the force to a black slime which is required to get to Blobby. This
// done by finding the shortest path to Blobby (using the A* algorithm),
// then following the path to Blobby. The a_changeState is used to indicate
// back to the StateMachine what state the state machine should switch to.
void FindState::Update(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	SlimeState &a_changeState,
	float deltaTime)
{
	// Firstly, set Blobby's position as the target vector.
	m_targetVector = m_p_blobby->m_p_agentMoveInfo->position;

	// Call the Basefind State update to move the black slime.
	BaseFindState::Update(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph,
		a_changeState,
		deltaTime);

	// Check for change of states.
	// When Blobby is spotted, the state will change.
	const Polygon *p_crate = a_p_entities->GetCrate();
	if (ColDetect::AreColliding(a_p_ownerAgent->m_p_sight,
		m_p_blobby->m_p_boundCircle))
	{
		// If Blobby is powered up, evade.
		if (m_p_blobby->IsPoweredUp())
		{
			a_changeState = SlimeState::EVADE;
		}
		else  // Pursue
		{
			a_changeState = SlimeState::PURSUE;
		}
	}
	// When a crate is spotted, switch to slow state.
	else if (p_crate != nullptr)
	{
		if (ColDetect::AreColliding(a_p_ownerAgent->m_p_sight, p_crate))
		{
			a_changeState = SlimeState::SLOW_FIND;
		}
	}
}

// Initialises the state. Find the shortest path to Blobby, then set the
// second node in the graph as the target to go to.
void FindState::Init(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph)
{
	// Set Blobby as the target
	m_p_blobby = a_p_entities->GetBlobby();
	m_targetVector = m_p_blobby->m_p_agentMoveInfo->position;

	BaseFindState::Init(a_p_entities, a_p_ownerAgent, a_p_worldGraph);

	a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 0.5f;
}

// Exit function. Cleans up all the memory used.
void FindState::Exit()
{
	// Set Blobby Pointer to null.
	m_p_blobby = nullptr;
	BaseFindState::Exit();
}


/*============================================================================
							SlowFind Class
=============================================================================*/

// Adds the force to a black slime which is required to get to Blobby. This
// done by finding the shortest path to Blobby (using the A* algorithm),
// then following the path to Blobby. The a_changeState is used to indicate
// back to the StateMachine what state the state machine should switch to.
void SlowFindState::Update(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	SlimeState &a_changeState,
	float deltaTime)
{
	// Firstly, set Blobby's position as the target vector.
	m_targetVector = m_p_blobby->m_p_agentMoveInfo->position;

	// Call the Basefind State update to move the black slime.
	BaseFindState::Update(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph,
		a_changeState,
		deltaTime);

	// Check for the change of states.
	// When Blobby is spotted, the state will change.
	const Polygon *p_crate = a_p_entities->GetCrate();
	if (ColDetect::AreColliding(a_p_ownerAgent->m_p_sight,
			m_p_blobby->m_p_boundCircle))
	{
		// If Blobby is powered up, evade.
		if (m_p_blobby->IsPoweredUp())
		{
			a_changeState = SlimeState::EVADE;
		}
		else  // Pursue
		{
			a_changeState = SlimeState::PURSUE;
		}
	}
	// When the Black Slime is a distance of 50 away from the crate, or the
	// crate dissapears, change back to find.
	else if (p_crate == nullptr)
	{
		a_changeState = SlimeState::FIND;
	}
	else if (glm::distance(a_p_ownerAgent->m_p_agentMoveInfo->position,
						   p_crate->m_centre) > 50.0f)
	{ 
		a_changeState = SlimeState::FIND;
	}
}

// Initialises the state. Find the shortest path to Blobby, then set the
// second node in the graph as the target to go to.
void SlowFindState::Init(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph)
{
	// Set Blobby as the target
	m_p_blobby = a_p_entities->GetBlobby();
	m_targetVector = m_p_blobby->m_p_agentMoveInfo->position;

	BaseFindState::Init(a_p_entities, a_p_ownerAgent, a_p_worldGraph);

	a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 0.1f;
}

// Exit function. Cleans up all the memory used.
void SlowFindState::Exit()
{
	// Set Blobby Pointer to null.
	m_p_blobby = nullptr;
	BaseFindState::Exit();
}

/*============================================================================
							PursueState Class
=============================================================================*/

// Adds the force to a black slime which is required to pursue Blobby. The
// a_changeState is used to indicate back to the StateMachine what state
// the state machine should switch to.
void PursueState::Update(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	SlimeState &a_changeState,
	float deltaTime)
{
	// Firstly, set Blobby's position as the target vector.
	m_targetVector = m_p_blobby->m_p_agentMoveInfo->position;

	// Call the Basefind State update to move the black slime.
	BaseFindState::Update(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph,
		a_changeState,
		deltaTime);

	// Determine the state to change to. If the slime can no longer see Blobby,
	// then switch to the Find state.
	if (!ColDetect::AreColliding(m_p_blobby->m_p_boundCircle,
		a_p_ownerAgent->m_p_sight))
	{
		a_changeState = SlimeState::FIND;
	}
	// Change to evade if Blobby reaches a crate while Pursuing.
	else if (m_p_blobby->IsPoweredUp())
	{
		a_changeState = SlimeState::EVADE;
	}
}

// Initialises the state. Sets Blobby as the target.
void PursueState::Init(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph)
{
	// Set Blobby as the target
	m_p_blobby = a_p_entities->GetBlobby();
	m_targetVector = m_p_blobby->m_p_agentMoveInfo->position;

	BaseFindState::Init(a_p_entities, a_p_ownerAgent, a_p_worldGraph);

	// Set the max speed to a higher value as it is chasing.
	a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 2.0f;
}

// Exit function. Cleans up all the memory used.
void PursueState::Exit()
{
	// Set the blobby pointer back to null.
	m_p_blobby = nullptr;
	BaseFindState::Exit();
}


/*============================================================================
							EvadeState Class
=============================================================================*/

// The Evade state for the black slime. This state is where the black slime
// runs from Blobby when she is spotted and powered up.
void EvadeState::Update(EntityManager *a_p_entities,
						Agent *a_p_ownerAgent,
						Graph *a_p_worldGraph,
					    SlimeState &a_changeState,
					    float deltaTime)
{
	// Set the target vector first.
	if (IsAimAtWall(a_p_entities, a_p_ownerAgent))
	{
		TargetWallEnd(a_p_ownerAgent);
	}
	else if (!IsAlongWall(a_p_entities, a_p_ownerAgent))
	{
		SetEvade(a_p_ownerAgent);
	}

	// Call the Basefind State update to move the black slime.
	BaseFindState::Update(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph,
		a_changeState,
		deltaTime);

	// Determine the state to change to. If Blobby is no longer powered up,
	// change to the find state.
	if (!m_p_blobby->IsPoweredUp())
	{
		a_changeState = SlimeState::FIND;
	}
}

// Initialises the state. Sets Blobby as the target.
void EvadeState::Init(EntityManager *a_p_entities,
					  Agent *a_p_ownerAgent,
					  Graph *a_p_worldGraph)
{
	// Get the location of Blobby
	m_p_blobby = a_p_entities->GetBlobby();

	// Set the target vector first.
	if (IsAimAtWall(a_p_entities, a_p_ownerAgent))
	{
		TargetWallEnd(a_p_ownerAgent);
	}
	else
	{
		SetEvade(a_p_ownerAgent);
	}

	BaseFindState::Init(a_p_entities, a_p_ownerAgent, a_p_worldGraph);

	// Set the max speed to a higher value as it is being chased.
	a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 2.0f;
}

// Exit function. Cleans up all the memory used.
void EvadeState::Exit()
{
	// Set the blobby and wall pointer back to null
	m_p_aheadWall = nullptr;
	m_p_blobby = nullptr;
}

// Determines whether the slime is aiming at a wall.
bool EvadeState::IsAimAtWall(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent)
{
	std::vector<Polygon*> walls = a_p_entities->GetWalls();

	for (auto &wall : walls)
	{
		// Check whether the ahead vector is colliding with the wall.
		if (ColDetect::AreColliding(a_p_ownerAgent->m_p_agentMoveInfo->position,
			a_p_ownerAgent->m_p_sight->m_centre,
			wall))
		{
			// Set the wall and return true.
			m_p_aheadWall = wall;
			return true;
		}
	}

	// Return false when no wall collide.
	return false;
}

// Determines whether the slime within a distance of a wall, and therefore
// traversing alongside it.
bool EvadeState::IsAlongWall(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent)
{
	std::vector<Polygon*> walls = a_p_entities->GetWalls();
	glm::vec2 slime = a_p_ownerAgent->m_p_agentMoveInfo->position;

	for (auto &wall : walls)
	{
		glm::vec2 wallVertex = wall->m_indices[0];
		// Check whether Blobby's position is within 150 pixels of a wall.
		if ((slime.x < wallVertex.x + 150.0f || slime.x > wallVertex.x - 150.0f) ||
			(slime.y < wallVertex.y + 150.0f || slime.y > wallVertex.y - 150.0f))
		{
			return true;
		}
	}

	// Return Failure when not within walls.
	return false;
}

// Sets a side of the wall that the slime is facing as the pathifinding
// target.
void EvadeState::TargetWallEnd(Agent *a_p_ownerAgent)
{
	// To get the ends of the wall, use two vertices that are in the opposite
	// corners of each other. As all polygons have been defined with each
	// vertex in a clockwise-direction, this means the 1st and 3rd vertext will
	// always be opposite each other.

	glm::vec2 endOne = m_p_aheadWall->m_indices[0];
	glm::vec2 endTwo = m_p_aheadWall->m_indices[2];

	// Get the wall segment line and wall start to Blobby vector.
	glm::vec2 wallLineSegment = endTwo - endOne;
	glm::vec2 segStartToBlob = (
		m_p_blobby->m_p_agentMoveInfo->position - endOne);

	// Project the wall start to blobby vector onto the wall segment vector by
	// getting the dot product of the segStartToSlime and normalised seg
	// vector.
	glm::vec2 unitSegVector = glm::normalize(wallLineSegment);
	float projectedDistance = glm::dot(segStartToBlob, unitSegVector);

	// Before scaling the unit seg vector to get the closest point, we need to
	// deal with two special cases. There is a possibility that the closest
	// point of the segment to blobby is one of the end points of the
	// segment. This can be determined by determining the projected distance.
	// If the projected distance is less than 0, the seg start is the closest
	// point: if it is greater than the magnitude of the segment vector, the the
	//segment end is the closest point. Otherwise, the closest point is found
	// by finding the vector which is scaled by the projectedDistance on the
	// seg unit vector, then adding this vector to the segment start.
	glm::vec2 closestPoint;
	if (projectedDistance < 0.0f)
	{
		closestPoint = endOne;
	}
	else if (projectedDistance > glm::length(wallLineSegment))
	{
		closestPoint = endTwo;
	}
	else
	{
		glm::vec2 closestVector = projectedDistance * unitSegVector;
		closestPoint = endOne + closestVector;
	}

	// Now, determine what side of the line the slime and the wall point ends
	// are in relation to Blobby to wall line segment. Use the sign of the
	// determinant of vectors to do this.
	float slimeResult = (
		((closestPoint.x - m_p_blobby->m_p_agentMoveInfo->position.x) *
		(a_p_ownerAgent->m_p_agentMoveInfo->position.y -
			m_p_blobby->m_p_agentMoveInfo->position.y)) -
		((closestPoint.y - m_p_blobby->m_p_agentMoveInfo->position.y) *
		(a_p_ownerAgent->m_p_agentMoveInfo->position.x -
			m_p_blobby->m_p_agentMoveInfo->position.x)));
	float endOneResult = (
		((closestPoint.x - m_p_blobby->m_p_agentMoveInfo->position.x) *
		(endOne.y - m_p_blobby->m_p_agentMoveInfo->position.y)) -
		((closestPoint.y - m_p_blobby->m_p_agentMoveInfo->position.y) *
		(endOne.x - m_p_blobby->m_p_agentMoveInfo->position.x)));
	float endTwoResult = (
		((closestPoint.x - m_p_blobby->m_p_agentMoveInfo->position.x) *
		(endTwo.y - m_p_blobby->m_p_agentMoveInfo->position.y)) -
		((closestPoint.y - m_p_blobby->m_p_agentMoveInfo->position.y) *
		(endTwo.x - m_p_blobby->m_p_agentMoveInfo->position.x)));

	// Pick the side which has the same sign (positive, negative) as blobby.
	if (slimeResult > 0)
	{
		if (endOneResult > 0)
		{
			m_targetVector = endOne;
		}
		else
		{
			m_targetVector = endTwo;
		}
	}
	else
	{
		if (endOneResult < 0)
		{
			m_targetVector = endOne;
		}
		else
		{
			m_targetVector = endTwo;
		}
	}

	// Move the targets in a little from the wall. Use the Screen Width and
	// Height (640x 480) to determine this.
	if (m_targetVector.x < 10.0f)
	{
		m_targetVector.x += 30.0f;
	}
	else if (m_targetVector.x > 630.0f)
	{
		m_targetVector.x -= 30.0f;
	}

	if (m_targetVector.y < 10.0f)
	{
		m_targetVector.y += 30.0f;
	}
	else if (m_targetVector.y > 470.0f)
	{
		m_targetVector.y -= 30.0f;
	}
}

// Sets a position 200 pixels away from Blobby as the evade target.
void EvadeState::SetEvade(Agent *a_p_ownerAgent)
{
	// Gte the vector from Blobby to the slime.
	glm::vec2 blobToSlime = (
		a_p_ownerAgent->m_p_agentMoveInfo->position -
		m_p_blobby->m_p_agentMoveInfo->position);

	// Normalise the vector and scale it by 200. Add that to the slime, and
	// we have set the evade target.
	blobToSlime = glm::normalize(blobToSlime);
	blobToSlime = blobToSlime * 200.0f;
	m_targetVector = (
		a_p_ownerAgent->m_p_agentMoveInfo->position + blobToSlime);
}
