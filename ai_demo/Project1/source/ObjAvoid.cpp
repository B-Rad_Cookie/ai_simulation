/*-----------------------------------------------------------------------------
file:			ObjAvoid.h
author:			Brad Cook
description:	This header file contains the definition of the ObjAvoid
				class: The class which implements the object collision
				avoidance AI Steering behaviour.
-----------------------------------------------------------------------------*/

#include "ObjAvoid.h"
#include "EntityManager.h"
#include "ColDetect.h"
#include "Circle.h"
#include "Polygon.h"
#include <glm\geometric.hpp>
#include <vector>

// The scalar avoidance force to apply
const float ObjAvoid::AVOIDANCE_FORCE = 32.0f;

// Returns the glm::vec2 force to apply to an agent such that the agent
// moves away from the closest obstacle.
glm::vec2 ObjAvoid::CalculateForce(const MovementInfo &a_agent,
	const EntityManager *a_entities)
{
	// First step is to find the most threatening obstacle. Go through all the
	// entities and find which obstacles the agents looking ahead collides
	// with.
	// The obstacles in this world which are to be avoided are the walls, water
	// and circle obstacles. When detecting for collisions, use the segment
	// from the agent's position to their ahead vector.
	// Keep a record of the obstacle that it is colliding with.
	const Polygon* p_collidePoly = nullptr;
	const Circle* p_collideCircle = nullptr;

	// Check with Water.
	const Polygon* p_water = a_entities->GetWater();
	if (ColDetect::AreColliding(a_agent.position, a_agent.ahead,
								p_water))
	{
		p_collidePoly = p_water;
	}

	// Check with Walls
	const std::vector<Polygon*> walls = a_entities->GetWalls();
	for (auto &wall : walls)
	{
		if (ColDetect::AreColliding(a_agent.position, a_agent.ahead,
									wall))
		{
			// It is not possible for an agent to be colliding with both a
			// wall and water with the ahead distance of 50.
			p_collidePoly = wall;
		}
	}
	
	// Check with obstacles.
	const std::vector<Circle*> obstacles = a_entities->GetObstacles();
	for (auto &obstacle : obstacles)
	{
		if (ColDetect::AreColliding(a_agent.position, a_agent.ahead,
									obstacle))
		{
			// It is not possible for an agent to be colliding with both a
			// wall and water with the ahead distance of 50.
			p_collideCircle = obstacle;
		}
	}

	// If no collisions, return (0, 0);
	if (p_collideCircle == nullptr && p_collidePoly == nullptr)
	{
		return glm::vec2(0.0f, 0.0f);
	}

	// If we have found a circle and polygon, need to determine which one is
	// closer to the agent.
	if (p_collideCircle != nullptr && p_collidePoly != nullptr)
	{
		if (ObjAvoid::IsPolyCloser(a_agent, p_collidePoly, p_collideCircle))
			p_collideCircle = nullptr;
		else
			p_collidePoly = nullptr;
	}

	// Calculate the relevant avoidance force.
	if (p_collideCircle != nullptr)
		return ObjAvoid::GetForce(a_agent, p_collideCircle);
	else
		return ObjAvoid::GetForce(a_agent, p_collidePoly);
	
}

// Determines whether the polygon a_p_poly is closer the the agent than the
// circle a_p_circle. Returns true if the poly is closer, returns false if
// the circle is closer.
bool ObjAvoid::IsPolyCloser(const MovementInfo &a_agent,
							const Polygon *a_p_poly,
							const Circle* a_p_circle)
{
	// Determine whether a geometry is closer by measuring the distance from
	// the agent's position to the geometry bounds.
	// For a circle, the distance is going to be the distance between the
	// agent and the centre of the circle minus the radius.
	float circleDistance = (
		glm::distance(a_agent.position, a_p_circle->m_centre) 
					  - a_p_circle->m_radius);

	// Find the polygon distance.
	float polyDistance = a_p_poly->DistanceToPoint(a_agent.position);

	// return the result.
	return polyDistance < circleDistance;
}


// Returns the avoidance force required for avoiding an obstacle.
glm::vec2 ObjAvoid::GetForce(const MovementInfo &a_agent,
							 const Circle* a_p_circle)
{
	// For a circle, the avoidance force is simply the vector from the circle
	// centre to the agent's look ahead. This vector is scaled to a constant
	// max force.
	glm::vec2 avoidForce = a_agent.position - a_p_circle->m_centre;
	avoidForce = glm::normalize(avoidForce);
	avoidForce = avoidForce * ObjAvoid::AVOIDANCE_FORCE;
	return avoidForce;
}

// Returns the avoidance force required for avoiding an obstacle.
glm::vec2 ObjAvoid::GetForce(const MovementInfo &a_agent,
							 const Polygon* a_p_poly)
{
	// For a polygon, we need to exert a force from the walls that are facing
	// the agent. For each edge, find the edge which is facing the agent by
	// finding the dot product. For an edge which will exert on the agent, get
	// the perpendicular of the wall (the direction of the force to exert) and
	// scale the normalised perpendicular force by a force scalar.

	// The force to return;
	glm::vec2 avoidForce = glm::vec2(0.0f, 0.0f);

	// Go through each edge.
	for (int i = 0; i < a_p_poly->m_indices.size(); i++)
	{
		// Get the other point of the edge.
		int endI = (i + 1) % a_p_poly->m_indices.size();

		// Get the edge vector and perpendicular.
		glm::vec2 edgeVector = (
			a_p_poly->m_indices[endI] - a_p_poly->m_indices[i]);
		glm::vec2 edgePerpVector = glm::vec2(edgeVector.y, -edgeVector.x);
		edgePerpVector = glm::normalize(edgePerpVector);

		// Get the vector of the agent to the edge's centre.
		glm::vec2 edgeCentre = glm::vec2(
			(a_p_poly->m_indices[endI].x + a_p_poly->m_indices[i].x) / 2.0f,
			(a_p_poly->m_indices[endI].y + a_p_poly->m_indices[i].y) / 2.0f);
		glm::vec2 agentToEdge = edgeCentre - a_agent.position;

		// Find the dot product of the agent to edge centre and the
		// perpendicular to edge vector. If the dot product is greater than or
		// equal to 0, the edge will not exert a force (dot product >= 0 means
		// vector is in the same direction).
		float dotProduct = glm::dot(agentToEdge, edgePerpVector);
		if (dotProduct < 0)
		{
			// Add the perpendicular force with a magnitude being the inverse
			// of the distance squared. 
			avoidForce += edgePerpVector * ObjAvoid::AVOIDANCE_FORCE;
		}
	}

	return avoidForce;
}
