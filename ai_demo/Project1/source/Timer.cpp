/*-----------------------------------------------------------------------------
file:			Timer.CPP
author:			Brad Cook
description:	This header file contains the definition of the Timer class:
				A class which keeps track of time.
-----------------------------------------------------------------------------*/

#include "Timer.h"

// Set Time to elapse.
Timer::Timer(float a_timeToElapse)
{
	m_setTime = a_timeToElapse;
	m_timeElapsed = 0.0f;
}

// Determines whether the time has elapsed.
bool Timer::TimeElapsed()
{
	return (m_timeElapsed >= m_setTime);
}

// Elapses time by the given amount (in seconds).
void Timer::AddTime(float a_time)
{
	m_timeElapsed += a_time;
}

// Resets the timer baack to 0
void Timer::Reset()
{
	m_timeElapsed = 0.0f;
}
