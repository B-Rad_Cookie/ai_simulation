/*-----------------------------------------------------------------------------
file:			Pursue.cpp
author:			Brad Cook
description:	This header file contains the definition of the Pursue class:
T				he class which implements the Pursue AI Steering behaviour.
-----------------------------------------------------------------------------*/

#include "Pursue.h"
#include <glm/geometric.hpp>


// Returns the glm::vec2 force to apply to an agent such that the agent
// moves to the target agent's velocity.
glm::vec2 Pursue::CalculateForce(const MovementInfo &a_agent,
								 const MovementInfo &a_target)
{
	// Do the Pursue.
	// Calculate a vector from the current position to the target.
	glm::vec2 toTargetVector = (
		(a_target.position + a_target.velocity) - a_agent.position);

	// Normalise the vector (divide it by it's magnitude).
	toTargetVector = glm::normalize(toTargetVector);

	// factor in the Agent's max Velocity.
	toTargetVector = toTargetVector * a_agent.maxSpeed;

	// Now get the force.
	glm::vec2 pursueForce = toTargetVector - a_agent.velocity;

	// Return the force to the velocity
	return pursueForce;
}
