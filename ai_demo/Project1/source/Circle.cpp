/*-----------------------------------------------------------------------------
file:			Circle.cpp
author:			Brad Cook
description:	This header file contains the definition of the Circle class:
				The class which represents a circle geometry; and,  The Well
				class: The class which represents a well (a specialised
				Circle).
-----------------------------------------------------------------------------*/

#include "Circle.h"
#include "Timer.h"

/*=============================================================================
								Circle Class
=============================================================================*/

// Circle Constructor. Sets the circle to the given centre and radius.
Circle::Circle(const glm::vec2 a_centre,
			   float a_radius)
{
	m_centre = a_centre;
	m_radius = a_radius;
}

/*=============================================================================
								Well Class
=============================================================================*/

// Well Constructor. Sets the bound circle, as well as initilaising the
// well to the given open or close state.
Well::Well(const glm::vec2 a_centre,
	float a_radius,
	bool a_isOpen) : Circle(a_centre, a_radius)
{
	m_isOpen = a_isOpen;
	m_p_reopenTimer = new Timer(30.0f);
}

// Determines whether the time to reopen has elapsed.
bool Well::TimeElapsed()
{
	return m_p_reopenTimer->TimeElapsed();
}

// Elapses time by the given amount (in seconds).
void Well::AddTime(float a_time)
{
	m_p_reopenTimer->AddTime(a_time);
}

// Resets the timer back to 0
void Well::Reset()
{
	m_p_reopenTimer->Reset();
}
