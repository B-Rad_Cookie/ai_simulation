/*-----------------------------------------------------------------------------
file:			Agent.cpp
author:			Brad Cook
description:	This cpp source file contains the declaration of the Agent
				class: The base class for all artificial agents.
-----------------------------------------------------------------------------*/

#include "Agent.h"
#include "Circle.h"
#include <glm\vec2.hpp>

/*=============================================================================
Agent class
*/

// Default Constructor
Agent::Agent()
{
	// Set the the physics info to 0 (except for max speed:
	// which is set to <blah>.
	m_p_agentMoveInfo = new MovementInfo;
	m_p_agentMoveInfo->position = glm::vec2(0.0f, 0.0f);
	m_p_agentMoveInfo->velocity = glm::vec2(0.0f, 0.0f);
	m_p_agentMoveInfo->acceleration = glm::vec2(0.0f, 0.0f);

	// Set the Line of Sight circle
	m_p_sight = new Circle(glm::vec2(0.0f, 0.0f), 75.0f);
}

// Constructor with X, Y starting position co-ordinates
Agent::Agent(const float a_positionX,
			 const float a_positionY) : Agent()
{
	m_p_agentMoveInfo->position.x = a_positionX;
	m_p_agentMoveInfo->position.y = a_positionY;
}

// Constructor with Vector2 starting position.
Agent::Agent(const glm::vec2 &a_position) : Agent()
{
	m_p_agentMoveInfo->position = a_position;
}

// Destructor
Agent::~Agent()
{
	// Delete the movement info.
	delete m_p_agentMoveInfo;

	// Delete Line of Sight
	delete m_p_sight;
}
