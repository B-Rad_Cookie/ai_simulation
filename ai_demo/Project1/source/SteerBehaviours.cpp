/*-----------------------------------------------------------------------------
file:			SteerBehaviours.cpp
author:			Brad Cook
description:	This cpp file contains the definitions of classes (nodes)
				used to perform steering behaviours (evade and pursue) as part
				of a Behaviour Tree. Note: These are NOT the actual steering
				themselves. See Pursue.cpp, Evade.cpp and Seek.cpp for those.
-----------------------------------------------------------------------------*/

#include "SteerBehaviours.h"
#include "BlackSlime.h"
#include "EntityManager.h"
#include "Blobby.h"
#include "ColDetect.h"
#include "Pursue.h"
#include "Evade.h"
#include <vector>
#include <glm\geometric.hpp>

/*=============================================================================
						InSightBehaviour Class.
=============================================================================*/

// Determines whether a black slime is in sight. Returns success if there
// is, returns failure otherwise.
Status InSightBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	std::vector<BlackSlime*> slimes = a_p_entities->GetSlimes();

	// Go through each and determine whether one is in sight. If there is, set
	// it as the target and change Blobby's speed.
	for (auto &slime : slimes)
	{
		if (ColDetect::AreColliding(a_p_ownerAgent->m_p_sight,
			slime->m_p_boundCircle))
		{
			a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = (
				slime->m_p_agentMoveInfo->position);
			a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 2.5f;

			return Status::BH_SUCCESS;
		}
	}
	// When none found, set speed to 1.0 and return failure.
	a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 1.0f;
	return Status::BH_FAILURE;
}
