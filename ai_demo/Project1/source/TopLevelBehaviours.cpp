/*-----------------------------------------------------------------------------
file:			TopLevelBehaviours.cpp
author:			Brad Cook
description:	This cpp file contains the definitions of classes (nodes)
				that are at the high end (close to the root) of the behaviour
				tree.
-----------------------------------------------------------------------------*/

#include "TopLevelBehaviours.h"
#include "Blobby.h"
#include "BlackSlime.h"
#include "EntityManager.h"
#include "ColDetect.h"
#include "Polygon.h"
#include "Timer.h"
#include <glm\geometric.hpp>

/*=============================================================================
						PowerUpBehaviour Class
=============================================================================*/

// Sets the powerup state of Blobby.
Status PowerUpBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	if (a_p_ownerAgent->IsPoweredUp() &&
		!a_p_ownerAgent->m_p_powerUpTimer->TimeElapsed())  // Still powered up
	{
		a_p_ownerAgent->m_p_powerUpTimer->AddTime(deltaTime);
		return Status::BH_SUCCESS;
	}

	if (a_p_ownerAgent->IsPoweredUp() &&
		a_p_ownerAgent->m_p_powerUpTimer->TimeElapsed())  // Ran out
	{
		a_p_ownerAgent->SetPowerUpState(false);
		a_p_ownerAgent->m_p_powerUpTimer->Reset();
		return Status::BH_SUCCESS;
	}

	// See whether Blobby has collided with the crate.
	const Polygon* p_crate = a_p_entities->GetCrate();

	if (p_crate == nullptr)
	{
		a_p_ownerAgent->SetPowerUpState(false);
		return Status::BH_SUCCESS;
	}

	if (ColDetect::AreColliding(a_p_ownerAgent->m_p_boundCircle, p_crate))
	{
		a_p_ownerAgent->SetPowerUpState(true);
		a_p_entities->RemoveCrate();
		return Status::BH_SUCCESS;
	}
	else
	{
		a_p_ownerAgent->SetPowerUpState(false);
		return Status::BH_SUCCESS;
	}
	return Status::BH_FAILURE;
}

/*=============================================================================
						IsPoweredBehaviour Class
=============================================================================*/

// Determines whether Blobby is powered up
Status IsPoweredBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	if (a_p_ownerAgent->IsPoweredUp())
	{
		// Set the evade find target to true, as Blobby is no longer evade
		// pathfinding.
		a_p_ownerAgent->m_p_treeInfo->m_newEvadeTarget = true;
		// Reset the evade timer and set timing to false.
		a_p_ownerAgent->m_p_treeInfo->m_timing = false;
		a_p_ownerAgent->m_p_treeInfo->m_p_evadeTime->Reset();
		return Status::BH_SUCCESS;
	}
	else
		return Status::BH_FAILURE;
}

/*=============================================================================
						SetSpeedBehaviour Class
=============================================================================*/

// Determines whether Blobby needs to evade, and set her max speed
// accordingly.
Status SetSpeedBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	/*
	Blobby will be prompted to evade if:
	1. If she spots a slime.
	2. If a slime gets within 120 pixels.

	Note that by this stage, it would already be determined that Blobby is not
	powered up.
	*/

	std::vector<BlackSlime*> slimes = a_p_entities->GetSlimes();

	for (auto &slime : slimes)
	{
		if (ColDetect::AreColliding(a_p_ownerAgent->m_p_sight,
			slime->m_p_boundCircle))
		{
			a_p_ownerAgent->m_p_treeInfo->m_p_evadeTime->Reset();
			a_p_ownerAgent->m_p_treeInfo->m_timing = true;
			a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 2.5f;
			return Status::BH_SUCCESS;
		}

		float dist = glm::distance(a_p_ownerAgent->m_p_agentMoveInfo->position,
			slime->m_p_agentMoveInfo->position);
		if (dist < 120.0f)
		{
			a_p_ownerAgent->m_p_treeInfo->m_p_evadeTime->Reset();
			a_p_ownerAgent->m_p_treeInfo->m_timing = true;
			a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 2.5f;
			return Status::BH_SUCCESS;
		}
	}
	// See if the evade timer is still ticking. Blobby is now out of view.
	if (a_p_ownerAgent->m_p_treeInfo->m_timing &&
		!a_p_ownerAgent->m_p_treeInfo->m_p_evadeTime->TimeElapsed())
	{
		a_p_ownerAgent->m_p_treeInfo->m_p_evadeTime->AddTime(deltaTime);
		a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 2.5f;
		return Status::BH_SUCCESS;
	}

	// Otherwise, no need to evade.
	a_p_ownerAgent->m_p_treeInfo->m_p_evadeTime->Reset();
	a_p_ownerAgent->m_p_treeInfo->m_timing = false;
	a_p_ownerAgent->m_p_agentMoveInfo->maxSpeed = 1.0f;
	return Status::BH_FAILURE;
}

/*=============================================================================
							Inverter Class
=============================================================================*/

// Swicthes the status state.
Status Inverter::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	Status childReturn = m_p_child->tick(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph,
		deltaTime);

	// If the return behaviour is Success, change to failure, and vice-versa.
	switch (childReturn)
	{
	case BH_SUCCESS:
		return Status::BH_FAILURE;
		break;
	case BH_FAILURE:
		return Status::BH_SUCCESS;
		break;
	default:
		return childReturn;
		break;
	}
}

/*=============================================================================
								Successor Class
=============================================================================*/

// Always returns success as the status.
Status Successor::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	Status childReturn = m_p_child->tick(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph,
		deltaTime);

	// Return Succes
	return Status::BH_SUCCESS;
}
