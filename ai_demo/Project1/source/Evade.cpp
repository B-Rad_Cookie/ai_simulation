/*-----------------------------------------------------------------------------
file:			Evade.cpp
author:			Brad Cook
description:	This header file contains the definition of the Evade class:
				The class which implements the Evade AI Steering behaviour.
-----------------------------------------------------------------------------*/

#include "Evade.h"
#include <glm/geometric.hpp>


// Returns the glm::vec2 force to apply to an agent such that the agent
// moves to the target agent's velocity.
glm::vec2 Evade::CalculateForce(const MovementInfo &a_agent,
								const MovementInfo &a_target)
{
	// Do the Evade.
	// Calculate a vector from the target's position and velocity to the
	// agent.
	glm::vec2 fromTargetVector = (
		a_agent.position - (a_target.position + a_target.velocity));

	// Normalise the vector (divide it by it's magnitude).
	fromTargetVector = glm::normalize(fromTargetVector);

	// factor in the Agent's max Velocity.
	fromTargetVector = fromTargetVector * a_agent.maxSpeed;

	// Now get the force.
	glm::vec2 evadeForce = fromTargetVector - a_agent.velocity;

	// Return the force to the velocity
	return evadeForce;
}