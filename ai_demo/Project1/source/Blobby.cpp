/*-----------------------------------------------------------------------------
file:			Blobby.h
author:			Brad Cook
description:	This header file contains the definition of the Blobby class:
				The class which represents the Blobby agent.
-----------------------------------------------------------------------------*/

#include "Blobby.h"
#include "Seek.h"
#include "Input.h"
#include "Circle.h"
#include "ObjAvoid.h"
#include "Selector.h"
#include "Sequence.h"
#include "TopLevelBehaviours.h"
#include "PathBehaviours.h"
#include "SteerBehaviours.h"
#include "LureBehaviours.h"
#include "TargetBehaviours.h"
#include "Timer.h"
#include <glm\geometric.hpp>

/*=============================================================================
						BehaviourTreeInfo struct
=============================================================================*/
// Constructor. Initialise everything to blank.
BehaviourTreeInfo::BehaviourTreeInfo()
{
	m_shortestPath = std::vector<Graph::Node*>();
	m_p_targetNode = nullptr;
	m_pathTargetVector = glm::vec2(-1.0f, -1.0f);  // Null.
	m_slimeVector = glm::vec2(-1.0f, -1.0f);  // Null.
	m_slimeToWellVector = glm::vec2(-1.0f, -1.0f);  // Null.
	m_newEvadeTarget = true;
	m_alongWall = false;
	m_timing = false;
	m_p_aheadWall = nullptr;
	m_openWell = glm::vec2(-1.0f, -1.0f);  // Null.
	m_pathIndex = 0;
	m_p_evadeTime = new Timer(5.0f);
}

// Destructor. Remove all memory created.
BehaviourTreeInfo::~BehaviourTreeInfo()
{
	m_shortestPath = std::vector<Graph::Node*>();
	m_p_targetNode = nullptr;
	m_pathTargetVector = glm::vec2(-1.0f, -1.0f);  // Null.
	m_slimeVector = glm::vec2(-1.0f, -1.0f);  // Null.
	m_slimeToWellVector = glm::vec2(-1.0f, -1.0f);  // Null.
	m_newEvadeTarget = true;
	m_alongWall = false;
	m_timing = false;
	m_p_aheadWall = nullptr;
	m_openWell = glm::vec2(-1.0f, -1.0f);  // Null.
	m_pathIndex = 0;
	delete m_p_evadeTime;
}

/*=============================================================================
								Blobby Class.
=============================================================================*/

// Default Constructor
Blobby::Blobby() : Agent()
{
	// Set the max speed to <blah>.
	m_p_agentMoveInfo->maxSpeed = 2.5f;

	// Set powered up to false
	m_isPoweredUp = false;

	// Set the bounds circle.
	m_p_boundCircle = new Circle(glm::vec2(0.0f, 0.0f), 15.0f);

	// Set the Power Up Timer.
	m_p_powerUpTimer = new Timer(20.0f);

	// Set the BehaviourTree information.
	m_p_treeInfo = new BehaviourTreeInfo();

	// Build the Behaviour Tree.
	m_p_rootBehaviour = new Sequence();
	PowerUpBehaviour *p_powerup = new PowerUpBehaviour();
	Selector *p_doBehaviour = new Selector();
	// Powerup Side
	Sequence *p_poweredUp = new Sequence();
	IsPoweredBehaviour *p_isPowered = new IsPoweredBehaviour();
	Sequence *p_doPoweredUp = new Sequence();
	Selector *p_getSlimeTarget = new Selector();
	Sequence *p_goToSlime = new Sequence();
	InSightBehaviour *p_inSightPursue = new InSightBehaviour();
	ClosestSlimeBehaviour *p_findSlime = new ClosestSlimeBehaviour();
	Selector *p_getSlimePath = new Selector();
	FollowPathBehaviour *p_slimeFollow = new FollowPathBehaviour();
	Inverter *p_needPathInvert = new Inverter();
	Sequence *p_calcSlimePath = new Sequence();
	NeedNewPathBehaviour *p_needPath = new NeedNewPathBehaviour();
	FindPathTargetBehaviour *p_findSlimeNode = new FindPathTargetBehaviour();
	FindPathBehaviour *p_runSlimePath = new FindPathBehaviour();

	// Non-Powered Up side
	Sequence *p_notPoweredUp = new Sequence();
	Successor *p_speedSuccess = new Successor();
	Selector *p_calcTarget = new Selector();
	Sequence *p_goToTarget = new Sequence();
	// Speed Branch.
	SetSpeedBehaviour *p_speed = new SetSpeedBehaviour();
	// Calc Target Branch
	// Find Crate Branch
	Sequence *p_crateTarg = new Sequence();
	CrateAvailableBehaviour *p_isCrate = new CrateAvailableBehaviour();
	TargetCrateBehaviour *p_forCrate = new TargetCrateBehaviour();
	// Find Lure Branch
	Sequence *p_calcLure = new Sequence();
	NearOpenWellBehvaiour *p_nearWell = new NearOpenWellBehvaiour();
	Sequence *p_getLureTarg = new Sequence();
	FindSlimeBehaviour *p_lureFindSlime = new FindSlimeBehaviour();
	SlimeToWellBehaviour *p_slimeToWell = new SlimeToWellBehaviour();
	GetLureTargetBehaviour *p_getLure = new GetLureTargetBehaviour();
	// Find Evade Target Branch
	Sequence *p_findEvade = new Sequence();
	IsEvadeBehaviour *p_isEvade = new IsEvadeBehaviour();
	Selector *p_getEvadeTarg = new Selector();
	// Target Along Wall Branch
	Sequence *p_alongWall = new Sequence();
	AimAtWallBehaviour *p_atWall = new AimAtWallBehaviour();
	FindSlimeBehaviour *p_wallSlime = new FindSlimeBehaviour();
	TargetEndWallBehaviour *p_getWalltarg = new TargetEndWallBehaviour();
	// Is wall following Branch
	FollowWallBehaviour *p_wallFollow = new FollowWallBehaviour();
	// Target Opposite Direction Branch
	Selector *p_targOpp = new Selector();
	Inverter *p_needTargInv = new Inverter();
	NewEvadeTargetBehaviour *p_needNewTarg = new NewEvadeTargetBehaviour();
	SetEvadeTargetBehaviour *p_calcEvadeTarg = new SetEvadeTargetBehaviour();
	// Last Crate Branch.
	SetLastCrateBehaviour *p_setLastCrate = new SetLastCrateBehaviour();
	// Go To Target Branch
	Selector *p_getTargPath = new Selector();
	FollowPathBehaviour *p_targFollow = new FollowPathBehaviour();
	Inverter *p_newPathInv = new Inverter();
	Sequence *p_calcTargPath = new Sequence();
	NeedNewPathBehaviour *p_targNeedPath = new NeedNewPathBehaviour();
	FindPathTargetBehaviour *p_findTargNode = new FindPathTargetBehaviour();
	FindPathBehaviour *p_runTargPath = new FindPathBehaviour();

	// The tree is built from top to bottom, left to right (Depth First
	// approach).
	m_p_rootBehaviour->addChild(p_powerup);
	m_p_rootBehaviour->addChild(p_doBehaviour);

	p_doBehaviour->addChild(p_poweredUp);
	p_doBehaviour->addChild(p_notPoweredUp);

	// Powerd Up Branch
	p_poweredUp->addChild(p_isPowered);
	p_poweredUp->addChild(p_doPoweredUp);

	p_doPoweredUp->addChild(p_getSlimeTarget);
	p_doPoweredUp->addChild(p_goToSlime);

	// Get Slime Branch
	p_getSlimeTarget->addChild(p_inSightPursue);
	p_getSlimeTarget->addChild(p_findSlime);

	// Go To Slime Branch.
	p_goToSlime->addChild(p_getSlimePath);
	p_goToSlime->addChild(p_slimeFollow);

	p_getSlimePath->addChild(p_needPathInvert);
	p_getSlimePath->addChild(p_calcSlimePath);

	p_needPathInvert->addChild(p_needPath);

	p_calcSlimePath->addChild(p_findSlimeNode);
	p_calcSlimePath->addChild(p_runSlimePath);

	// Not Powered Up branch.
	p_notPoweredUp->addChild(p_speedSuccess);
	p_notPoweredUp->addChild(p_calcTarget);
	p_notPoweredUp->addChild(p_goToTarget);

	p_speedSuccess->addChild(p_speed);
	
	// Calc Target Branch
	p_calcTarget->addChild(p_crateTarg);
	p_calcTarget->addChild(p_calcLure);
	p_calcTarget->addChild(p_findEvade);
	p_calcTarget->addChild(p_setLastCrate);

	// Find Crate Branch
	p_crateTarg->addChild(p_isCrate);
	p_crateTarg->addChild(p_forCrate);

	// Find Lure Branch
	p_calcLure->addChild(p_nearWell);
	p_calcLure->addChild(p_getLureTarg);

	p_getLureTarg->addChild(p_lureFindSlime);
	p_getLureTarg->addChild(p_slimeToWell);
	p_getLureTarg->addChild(p_getLure);

	// Find Evade Target Branch
	p_findEvade->addChild(p_isEvade);
	p_findEvade->addChild(p_getEvadeTarg);

	p_getEvadeTarg->addChild(p_alongWall);
	p_getEvadeTarg->addChild(p_wallFollow);
	p_getEvadeTarg->addChild(p_targOpp);

	// Target Along Wall Branch
	p_alongWall->addChild(p_atWall);
	p_alongWall->addChild(p_wallSlime);
	p_alongWall->addChild(p_getWalltarg);

	// Target Opposite Direction Branch
	p_targOpp->addChild(p_needTargInv);
	p_targOpp->addChild(p_calcEvadeTarg);

	p_needTargInv->addChild(p_needNewTarg);

	// Go To Target Branch
	p_goToTarget->addChild(p_getTargPath);
	p_goToTarget->addChild(p_targFollow);

	p_getTargPath->addChild(p_newPathInv);
	p_getTargPath->addChild(p_calcTargPath);

	p_newPathInv->addChild(p_targNeedPath);

	p_calcTargPath->addChild(p_findTargNode);
	p_calcTargPath->addChild(p_runTargPath);
}

// Constructor with X, Y starting position co-ordinates
Blobby::Blobby(float a_positionX,
			   float a_positionY) : Blobby()
{
	m_p_agentMoveInfo->position.x = a_positionX;
	m_p_agentMoveInfo->position.y = a_positionY;
}

// Constructor with Vector2 starting position.
Blobby::Blobby(const glm::vec2 &a_position) : Blobby()
{
	m_p_agentMoveInfo->position = a_position;
	m_p_boundCircle->m_centre = a_position;
}

// Destructor
Blobby::~Blobby()
{
	delete m_p_treeInfo;
	delete m_p_boundCircle;
	delete m_p_rootBehaviour;
	delete m_p_powerUpTimer;
}

// Update the BlackSlime information.
void Blobby::Update(EntityManager *a_p_entities,
					Graph *a_p_worldGraph,
					float deltaTime)
{

	// Get the collision obstacle avoidance.
	glm::vec2 avoidanceForce = (ObjAvoid::CalculateForce(*m_p_agentMoveInfo,
		a_p_entities) * deltaTime);

	// Run the Behaviour Tree.
	m_p_rootBehaviour->tick(a_p_entities, this, a_p_worldGraph, deltaTime);

	/*
	If the obstacle avoidance returns a force that isn't vec2(0, 0), then
	weight the obstacle avoidance based on the agent's max speed. 
	The faster the agent is moving, the greater the importance of the avoidance
	force. The max weight for the avoidance will be 0.1f. The absolute max
	Blobby can travel is 2.5f. Therefore, to caculate the avoidance weight, it
	will be the max speed divided by 25.0f (ten times the absolute max speed).
	*/

	if (avoidanceForce != glm::vec2(0.0f, 0.0f))
	{
		float avoidWeight = m_p_agentMoveInfo->maxSpeed / 25.0f;
		float behaviourWeight = 1.0f - avoidWeight;
		m_p_agentMoveInfo->acceleration *= behaviourWeight;
		m_p_agentMoveInfo->acceleration += avoidanceForce * avoidWeight;
	}

	// Update the agent information
	// Update the velocity with the current acceleration
	m_p_agentMoveInfo->velocity += m_p_agentMoveInfo->acceleration;

	// constrain the velocity
	if (m_p_agentMoveInfo->velocity.x > m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.x = m_p_agentMoveInfo->maxSpeed;
	}
	else if (m_p_agentMoveInfo->velocity.x < -m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.x = -m_p_agentMoveInfo->maxSpeed;
	}
	if (m_p_agentMoveInfo->velocity.y > m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.y = m_p_agentMoveInfo->maxSpeed;
	}
	else if (m_p_agentMoveInfo->velocity.y < -m_p_agentMoveInfo->maxSpeed)
	{
		m_p_agentMoveInfo->velocity.y = -m_p_agentMoveInfo->maxSpeed;
	}

	// reset the acceleration for the next frame
	m_p_agentMoveInfo->acceleration = glm::vec2(0.0f, 0.0f);

	// add the velocity to the position
	m_p_agentMoveInfo->position += m_p_agentMoveInfo->velocity;
	m_p_boundCircle->m_centre += m_p_agentMoveInfo->velocity;

	// update the agent look ahead
	m_p_agentMoveInfo->ahead = (
		m_p_agentMoveInfo->position +
		((glm::normalize(m_p_agentMoveInfo->velocity) * 50.0f)));

	// update line of sight.
	m_p_sight->m_centre = (
		m_p_agentMoveInfo->position +
		((glm::normalize(m_p_agentMoveInfo->velocity) * 75.0f)));
}
