/*-----------------------------------------------------------------------------
file:			Polygon.cpp
author:			Brad Cook
description:	This header file contains the declaration of the Polygon
				class: The class which represents a polygon geometry
-----------------------------------------------------------------------------*/

#include "Polygon.h"
#include "SpriteBatch.h"
#include <glm\geometric.hpp>
#include <vector>
#include <algorithm>

// Indices Constructor. Create a new Polygon with the listed indices and
// central point. The indices must be specified in a clockwise-direction.
Polygon::Polygon(const std::vector<glm::vec2> &a_indices,
				 const glm::vec2 &a_centre)
{
	m_indices = std::vector<glm::vec2>(a_indices);
	m_centre = a_centre;
}

// Finds the distance from the polygon to a point.
float Polygon::DistanceToPoint(const glm::vec2 &a_point) const
{
	/* 
	For a polygon, we need to find the minimum distance from all the polygon
	edges to the point. The minimum of all of these distances will be the
	distance from the polygon to the point.

	To find the minimum distance to a line segment, we need to find the closest
	point on the edge to the point. We do this by projecting the vector of the
	edge endpoint onto the normalised edge vector (dot product). If the
	resulting dot product < 0, then the chosen endpoint is the closest point on
	the edge to the point. If the resulting dot product > edge vector
	magnitude, then the opposite endpoint is the closest point on the edge to
	the point. Otherwise, the closest point on the edge to the point is found
	by scaling the edge unit (normalised) vector by the dot product and adding
	the resultant vector to the start edge point. When the closest point is
	found, we then use it to find the distance to the point.
	*/

	// Store all the results in a vector
	std::vector<float> edgeDistances = std::vector<float>();

	// Go through each edge.
	for (int i = 0; i < m_indices.size(); i++)
	{
		// Get the other point of the edge.
		int otherPoint = (i + 1) % m_indices.size();

		// Get the edge vector and edge start to point.
		glm::vec2 edgeVector = m_indices[otherPoint] - m_indices[i];
		glm::vec2 edgeStartToCentre = m_indices[i] - a_point;

		// Project the seg start to centre vector onto the segment vector by
		// getting the dot product of the segStartToCentre and normalised seg
		// vector.
		glm::vec2 unitEdgeVector = glm::normalize(edgeVector);
		float projectedDistance = glm::dot(edgeStartToCentre, unitEdgeVector);

		// Determine closest point (using logic described above).
		glm::vec2 closestPoint;
		if (projectedDistance < 0.0f)
		{
			closestPoint = m_indices[i];
		}
		else if (projectedDistance > glm::length(edgeVector))
		{
			closestPoint = m_indices[otherPoint];
		}
		else
		{
			glm::vec2 closestVector = projectedDistance * unitEdgeVector;
			closestPoint = m_indices[i] + closestVector;
		}

		// Find the distance to the point, then add result to the list of
		// resutls.
		float edgeToPointDistance = glm::distance(closestPoint, a_point);
		edgeDistances.push_back(edgeToPointDistance);
	}

	// Return the minimum of all the edge distances.
	return *std::min_element(edgeDistances.begin(), edgeDistances.end());
}
