/*-----------------------------------------------------------------------------
file:			ColDetect.cpp
author:			Brad Cook
description:	This header file contains the declaration of the ColDetect
				class: The utility class which handles collision detection of
				geometry objects.
-----------------------------------------------------------------------------*/

#include "ColDetect.h"
#include "Circle.h"
#include "Polygon.h"
#include <glm\geometric.hpp>
#include <algorithm>

// Determines whether two circles are intersecting.
bool ColDetect::AreColliding(const Circle* a_p_c1, const Circle* a_p_c2)
{
	/*
	To determine whether 2 circles are intersecting, rather than use SAT,
	The way to determine whether circles intersect is to find the distance
	between the two centre points and see whether that distance is greater than
	the sum of radius. If the distance is less than the sum, they intersect.
	*/

	// Get the distance between the centres.
	float centreDistance = glm::distance(a_p_c1->m_centre, a_p_c2->m_centre);

	// Get the sums of the radii.
	float radiusSum = a_p_c1->m_radius + a_p_c2->m_radius;

	// Return the result.
	if (centreDistance <= radiusSum)
		return true;
	else
		return false;
}

// Determines whether a point falls within a circle.
bool ColDetect::AreColliding(const glm::vec2 &a_point,
							 const Circle* a_p_circle)
{
	/*
	To determine whether a point is within a circle are intersecting, find the
	distance between the point and the centre circle and see whether that
	distance is greater than the radius. If the distance is less than the
	radius, they intersect.
	*/

	// Get the distance between the point and centre.
	float distance = glm::distance(a_point, a_p_circle->m_centre);

	// Return the result.
	if (distance <= a_p_circle->m_radius)
		return true;
	else
		return false;
}

// Determines whether a segment intersects a circle.
bool ColDetect::AreColliding(const glm::vec2 &a_seg_start,
	const glm::vec2 &a_seg_end,
	const Circle* a_p_circle)
{
	/*
	For a line segment and circle, find the closest point on the segment to the
	circle. This can be done by first finding the vector from the line_segment
	start to the centre of the circle, then projecting this vector onto the
	line segment vector. Onec we have the closest, simply test whether the
	distance from the closest point to the centre is less than the circle's
	radius.
	*/

	// Get the segment vector and segment start to circle centre vector.
	glm::vec2 segmentVector = a_seg_end - a_seg_start;
	glm::vec2 segStartToCentre = a_p_circle->m_centre - a_seg_start;

	// Project the seg start to centre vector onto the segment vector by
	// getting the dot product of the segStartToCentre and normalised seg
	// vector.
	glm::vec2 unitSegVector = glm::normalize(segmentVector);
	float projectedDistance = glm::dot(segStartToCentre, unitSegVector);

	// Before scaling the unit seg vector to get the closest point, we need to
	// deal with two special cases. There is a possibility that the closest
	// point of the segment to the centre is one of the end points of the
	// segment. This can be determined by determining the projected distance.
	// If the projected distance is less than 0, the seg start is the closest
	// point: if it is greater than the magnitude of the segment vector, the the
	//segment end is the closest point. Otherwise, the closest point is found
	// by finding the vector which is scaled by the projectedDistance on the
	// seg unit vector, then adding this vector to the segment start.
	glm::vec2 closestPoint;
	if (projectedDistance < 0.0f)
	{
		closestPoint = a_seg_start;
	}
	else if (projectedDistance > glm::length(segmentVector))
	{
		closestPoint = a_seg_end;
	}
	else
	{
		glm::vec2 closestVector = projectedDistance * unitSegVector;
		closestPoint = a_seg_start + closestVector;
	}

	// See whether the distance between the closest point and circle is less
	// than the circle's radius.
	float closestToCentreDistance = glm::distance(
		closestPoint, a_p_circle->m_centre);

	if (closestToCentreDistance < a_p_circle->m_radius)
		return true;
	else
		return false;
}

// Determines whether a point falls within a Polygon.
bool ColDetect::AreColliding(const glm::vec2 &a_point,
	const Polygon* a_p_poly)
{
	/*
	Use the separate axis thereom. However, as a point has no axes, only the
	polygon will be used to determine axes of separation.
	*/

	std::vector<glm::vec2> axesOfSeparation = std::vector<glm::vec2>();

	// Make a std::vector out of the point to use with the SAT.
	std::vector<glm::vec2> point = { a_point };

	// Find the axes of separation for the polygon.
	ColDetect::GetAxesOfSeparation(axesOfSeparation, a_p_poly->m_indices);

	// Return the Separate Axis Thereom
	return ColDetect::SeparateAxisThereom(axesOfSeparation,
										  point,
										  a_p_poly->m_indices);
}

// Determines whether a segment falls within a polygon.
bool ColDetect::AreColliding(const glm::vec2 &a_seg_start,
							 const glm::vec2 &a_seg_end,
							 const Polygon* a_p_polygon)
{
	/*
	Use the Separate Axis Thereom. 
	*/

	std::vector<glm::vec2> axesOfSeparation = std::vector<glm::vec2>();

	// Make a std::vector out of the line segment to use with the SAT.
	std::vector<glm::vec2> segment = { a_seg_start, a_seg_end };

	// Find the axes of separation. Inlcude the segment, as it does have an
	// axis.
	ColDetect::GetAxesOfSeparation(axesOfSeparation, segment);
	ColDetect::GetAxesOfSeparation(axesOfSeparation, a_p_polygon->m_indices);

	// Return the SAT
	return ColDetect::SeparateAxisThereom(axesOfSeparation,
									      segment,
									      a_p_polygon->m_indices);
}

// Determines whether a circle falls within a polygon.
bool ColDetect::AreColliding(const Circle* a_p_circle,
							 const Polygon* a_p_polygon)
{
	/*
	With the circle and polygon, we will be using separate axis thereom.
	However, a circle is differnt to the other shapes because it has infinite
	axes. 
	
	The way we approach this is having the polygon edges form the axes
	of separation. When we project each index onto an axis of separation, for
	the circle, we will project on the centre circle, then take the + and -
	radius from the projected centre as the min and max for that axis.
	*/

	std::vector<glm::vec2> axesOfSeparation = std::vector<glm::vec2>();

	// Find the axes of separation. Inlcude the segment, as it does have an
	// axis.
	ColDetect::GetAxesOfSeparation(axesOfSeparation, a_p_polygon->m_indices);

	// Implement the Separate Axis Thereom here using the process described
	// above.
	for (auto &separationAxis : axesOfSeparation)
	{
		// Project each vertex onto the axis of separation.
		std::vector<float> polyProjs = std::vector<float>();
		for (auto &edge : a_p_polygon->m_indices)
		{
			polyProjs.push_back(glm::dot(edge, separationAxis));
		}

		// Find the max and min value for the poly shapes.
		auto polyMinMax = std::minmax_element(polyProjs.begin(),
			polyProjs.end());
		float polyMin = *polyMinMax.first;
		float polyMax = *polyMinMax.second;

		// Now project the circle centre.
		float projCircle = glm::dot(a_p_circle->m_centre, separationAxis);

		// + and - the radius to get it's min and max.
		float circleMin = projCircle - a_p_circle->m_radius;
		float circleMax = projCircle + a_p_circle->m_radius;

		// Find the overlap.
		glm::vec2 overlap = glm::vec2(polyMax - circleMin,
									  circleMax - polyMin);

		// Check if there is negative overlap. If there is, the shapes don't
		// overlap.
		if (overlap.x < 0 || overlap.y < 0)
			return false;
	}
	// When all axes have been checked and there is overlap, they do overlap,
	// so return true.
	return true;
}

// Determines whether a polygon falls within a polygon.
bool ColDetect::AreColliding(const Polygon* a_p_poly1,
						 const Polygon* a_p_poly2)
{
	/*
	For two polygons, use the Separate Axis Thereom. Therefore the first step
	is to find all the axes of spearation. These are simply the unit vectors of
	all the sides of a polygon.
	*/

	std::vector<glm::vec2> axesOfSeparation = std::vector<glm::vec2>();

	// Find the axes of separation from both polygons.
	ColDetect::GetAxesOfSeparation(axesOfSeparation, a_p_poly1->m_indices);
	ColDetect::GetAxesOfSeparation(axesOfSeparation, a_p_poly2->m_indices);

	// Return the Separate Axis Thereom algorithm.
	return ColDetect::SeparateAxisThereom(axesOfSeparation,
										  a_p_poly1->m_indices,
										  a_p_poly2->m_indices);
}

/******************************************************************************
					Separating Axis Thereom Algorithm
******************************************************************************/

// Adds the axis from the indices to the axes of separation.
void ColDetect::GetAxesOfSeparation(std::vector<glm::vec2> &a_AOS,
									const std::vector<glm::vec2> &a_indices)
{
	/*
	If any of the axes of separation are equivalent(i.e.they have the same
	axes), only record one of them to save time on the equation.
	*/

	// For each of the polygon sides, calculate the vector, then it's
	// normalised vector, and add it to the list of axes of separation.
	// As we need the start and end points of a polygon side, keep an array
	// index to the end point.
	int polygonEdgeEnd = 1;
	for (int i = 0; i < a_indices.size(); i++)
	{
		glm::vec2 edgeVector = (a_indices[polygonEdgeEnd] - a_indices[i]);
		edgeVector = glm::normalize(edgeVector);

		// Check whether this axis is the same as one already in the axes of
		// separation. As well as checking for vector equivalency, also check
		// whether the opposite direction vector of the edge is in the axes, as
		// they are the same axes in opposite directions.
		if (a_AOS.empty())
		{
			a_AOS.push_back(edgeVector);
		}
		else
		{
			// Check to see if edge vector is already recorded as an axis of
			// separation.
			if (std::find(a_AOS.begin(), a_AOS.end(), edgeVector)
				== a_AOS.end())
			{
				// Check to see if opposite direction edge vector is already
				// recorded as an axis of separation.
				if (std::find(a_AOS.begin(), a_AOS.end(), edgeVector * -1.0f)
					== a_AOS.end())
				{
					a_AOS.push_back(edgeVector);
				}
			}
		}

		// Wrap the polygonEdgeEnd around the array by using the modulus
		// operator.
		polygonEdgeEnd = (polygonEdgeEnd + 1) % a_indices.size();
	}
}

// Determines whether two shapes, specified as indices, collide using the
// Separate Axis Thereom. reutnrs true if they do, returns false otherwise.
bool ColDetect::SeparateAxisThereom(
	const std::vector<glm::vec2> &a_axesOfSeparation,
	const std::vector<glm::vec2> &a_shapeOneIndices,
	const std::vector<glm::vec2> &a_shapeTwoIndices)
{
	/*
	Separate Axis Thereom works by checking whether 2 shapes overalp on every
	axis of separation. To do this, for each axis of separation, project every
	index onto the axis. Get the min and max projections for the two shapes.
	Then, see whether the min max bounds of the two shapes overlap. As soon as
	they don't overlap on one axis, return false. If they all overlap, return
	true.

	In this implementation, The minimum translation vector is not being sort.
	*/

	for (auto &separationAxis : a_axesOfSeparation)
	{
		// Project each vertex onto the axis of separation.
		std::vector<float> shapeOneProjs = std::vector<float>();
		for (auto &edge : a_shapeOneIndices)
		{
			shapeOneProjs.push_back(glm::dot(edge, separationAxis));
		}
		std::vector<float> shapeTwoProjs = std::vector<float>();
		for (auto &edge : a_shapeTwoIndices)
		{
			shapeTwoProjs.push_back(glm::dot(edge, separationAxis));
		}

		// Find the max and min value for both shapes.
		auto shapeOneMinMax = std::minmax_element(shapeOneProjs.begin(),
												  shapeOneProjs.end());
		float shapeOneMin = *shapeOneMinMax.first;
		float shapeOneMax = *shapeOneMinMax.second;

		auto shapeTwoMinMax = std::minmax_element(shapeTwoProjs.begin(),
												  shapeTwoProjs.end());
		float shapeTwoMin = *shapeTwoMinMax.first;
		float shapeTwoMax = *shapeTwoMinMax.second;

		// Find the overlap.
		glm::vec2 overlap = glm::vec2(shapeOneMax - shapeTwoMin,
									  shapeTwoMax - shapeOneMin);

		// Check if there is negative overlap. If there is, the shapes don't
		// overlap.
		if (overlap.x < 0 || overlap.y < 0)
			return false;
	}
	// When all axes have been checked and there is overlap, they do overlap,
	// so return true.
	return true;
}
