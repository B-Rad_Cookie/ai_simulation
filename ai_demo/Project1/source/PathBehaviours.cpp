/*-----------------------------------------------------------------------------
file:			PathBehaviours.cpp
author:			Brad Cook
description:	This header file contains the definitions of classes (nodes)
				used to perform Pathfinding behaviours as part of a Behaviour
				Tree.
-----------------------------------------------------------------------------*/

#include "PathBehaviours.h"
#include "EntityManager.h"
#include "AStar.h"
#include "BlackSlime.h"
#include "Blobby.h"
#include "Polygon.h"
#include "ColDetect.h"
#include "Seek.h"
#include <glm\geometric.hpp>

/*=============================================================================
					ClosestSlimeBehaviour Class.
=============================================================================*/

// Finds the closest black slime. "Closest" is defined as the Euclidean
// distance.
Status ClosestSlimeBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Get the squared distance between Blobby and the slimes and see which
	// is the closest.
	float minDist = INFINITY;
	std::vector<BlackSlime*> slimes = a_p_entities->GetSlimes();
	for (auto &slime : slimes)
	{
		float dist = glm::dot(a_p_ownerAgent->m_p_agentMoveInfo->position,
			slime->m_p_agentMoveInfo->position);

		if (dist < minDist)
		{
			a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector = (
				slime->m_p_agentMoveInfo->position);
			minDist = dist;
		}
	}
	if (minDist < INFINITY)
		return Status::BH_SUCCESS;
	else
		return Status::BH_FAILURE;
}

/*=============================================================================
					NeedNewPathBehaviour Class.
=============================================================================*/

// Determines whether a new path is needed to get to the target.
// If a new path is needed, success will be returned, otherwise failure
// will be returned when not needed.
Status NeedNewPathBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	/*
	A new path is needed if:
	1. The target hasn't been set
	2. Blobby has reached the end
	3. There is no shortestPath.
	4. Blobby has changed from her graph node.
	5. The target has changed to a different node (whether by moving [black
	slime] or changing target [targeting the crate]). 
	
	This avoids pathfinding being run every frame.
	*/

	// Check the first condition.
	if (a_p_ownerAgent->m_p_treeInfo->m_p_targetNode == nullptr)
		return Status::BH_SUCCESS;

	// Check whether the agent has reached the end. This can be done by
	// checking whether the path index and size of the shortest path are the
	// same.
	if (a_p_ownerAgent->m_p_treeInfo->m_pathIndex >=
		a_p_ownerAgent->m_p_treeInfo->m_shortestPath.size() - 1)
		return Status::BH_SUCCESS;

	if (a_p_ownerAgent->m_p_treeInfo->m_shortestPath.empty())
		return Status::BH_SUCCESS;

	// Check whether Blobby's current node is within the path.

	// Check whether Blobby has moved form the initial node.
	Graph::Node *p_blobNode = a_p_worldGraph->FindNearestNode(
		a_p_ownerAgent->m_p_agentMoveInfo->position.x,
		a_p_ownerAgent->m_p_agentMoveInfo->position.y);
	if (*p_blobNode != *a_p_ownerAgent->m_p_treeInfo->m_shortestPath[0])
		return Status::BH_SUCCESS;

	Graph::Node *p_node = a_p_worldGraph->FindNearestNode(
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.x,
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.y);

	// This test checks the remaining conditions. If the target has changed,
	// the target node will. If the change in target happens to be on the same
	// node, then there's no need to recalculate a path. 
	if (*p_node != *a_p_ownerAgent->m_p_treeInfo->m_p_targetNode)
	{
		return Status::BH_SUCCESS;
	}
	return BH_FAILURE;
}

/*=============================================================================
					FindPathTargetBehaviour Class.
=============================================================================*/

// Finds the Path finding target node.
Status FindPathTargetBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	a_p_ownerAgent->m_p_treeInfo->m_p_targetNode = (
		a_p_worldGraph->FindNearestNode(
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.x,
		a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector.y));
	
	if (a_p_ownerAgent->m_p_treeInfo->m_p_targetNode == nullptr)
		return Status::BH_FAILURE;

	return Status::BH_SUCCESS;
}

/*=============================================================================
						FindPathBehaviour Class.
=============================================================================*/

// Runs the Path finding algorithm.
Status FindPathBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	AStar *p_astar = new AStar();
	// Get the node closest to Blobby.
	Graph::Node *p_blobbyLoc = a_p_worldGraph->FindNearestNode(
		a_p_ownerAgent->m_p_agentMoveInfo->position.x,
		a_p_ownerAgent->m_p_agentMoveInfo->position.y);
	if (p_blobbyLoc == nullptr)
	{
		delete p_astar;
		return Status::BH_FAILURE;
	}

	p_astar->AStarShortestPath(*a_p_worldGraph,
		p_blobbyLoc,
		a_p_ownerAgent->m_p_treeInfo->m_p_targetNode,
		a_p_ownerAgent->m_p_treeInfo->m_shortestPath);
	// Set target to first Node.
	a_p_ownerAgent->m_p_treeInfo->m_pathIndex = 0;

	delete p_astar;
	return Status::BH_SUCCESS;
}

/*=============================================================================
						FollowPathBehaviour Class.
=============================================================================*/

// Makes Blobby move along the path.
Status FollowPathBehaviour::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// If Blobby and the target node are the same, set the target as the
	// slime itslef.
	if (a_p_ownerAgent->m_p_treeInfo->m_shortestPath.empty())
	{
		// Add the force required to move to the target
		a_p_ownerAgent->m_p_agentMoveInfo->acceleration += (
			Seek::CalculateForce(
				*a_p_ownerAgent->m_p_agentMoveInfo,
				a_p_ownerAgent->m_p_treeInfo->m_pathTargetVector) * deltaTime);
		return Status::BH_SUCCESS;
	}

	// Get the current path node position
	glm::vec2 targetNodeVec = glm::vec2(
		a_p_ownerAgent->m_p_treeInfo->m_shortestPath[
			a_p_ownerAgent->m_p_treeInfo->m_pathIndex]->m_x,
		a_p_ownerAgent->m_p_treeInfo->m_shortestPath[
			a_p_ownerAgent->m_p_treeInfo->m_pathIndex]->m_y);

	// Check whether Blobby has reached the target node. If so, set target
	// as next node.
	if (ColDetect::AreColliding(targetNodeVec,
		a_p_ownerAgent->m_p_boundCircle))
	{
		a_p_ownerAgent->m_p_treeInfo->m_pathIndex++;

		targetNodeVec = glm::vec2(
			a_p_ownerAgent->m_p_treeInfo->m_shortestPath[
				a_p_ownerAgent->m_p_treeInfo->m_pathIndex]->m_x,
			a_p_ownerAgent->m_p_treeInfo->m_shortestPath[
				a_p_ownerAgent->m_p_treeInfo->m_pathIndex]->m_y);
	}

	// Add the force required to move to the target
	a_p_ownerAgent->m_p_agentMoveInfo->acceleration += (Seek::CalculateForce(
		*a_p_ownerAgent->m_p_agentMoveInfo, targetNodeVec) * deltaTime);

	return Status::BH_SUCCESS;
}
