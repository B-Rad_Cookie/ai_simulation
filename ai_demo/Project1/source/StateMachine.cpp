/*-----------------------------------------------------------------------------
file:			StateMachine.cpp
author:			Brad Cook
description:	This cpp file contains the definition of the StateMachine
				class: This is the Finite State Machine (FSM) for the Black
				Slime agents.
-----------------------------------------------------------------------------*/

#include "StateMachine.h"
#include "States.h"
#include "IState.h"

// Constructor which Sets up a new StateMachine instance. The first state
// is set to FIND.
StateMachine::StateMachine(EntityManager *a_p_entities,
	Agent *a_p_ownerAgent,
	Graph *a_p_worldGraph)
{
	// Set the first state pointer as Find and the previous state as null
	m_p_currentState = new FindState();
	m_p_previousState = nullptr;

	// Initialise the state
	m_p_currentState->Init(a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph);
}

// Destrcutor
StateMachine::~StateMachine()
{
	// Free up the space used by previous and current states.
	delete m_p_currentState;
	delete m_p_previousState;
}

// Applies the current state's force to the owner agent. It will calls the
// current state's update function to calculate the force.
void StateMachine::Update(EntityManager *a_p_entities,
						  Agent *a_p_ownerAgent,
						  Graph *a_p_worldGraph,
						  float deltaTime)
{
	// Set the change state to NO_CHANGE.
	SlimeState changeState = SlimeState::NO_CHANGE;

	// call the current state's update function.
	m_p_currentState->Update(
		a_p_entities, a_p_ownerAgent,
		a_p_worldGraph,
		changeState, deltaTime);

	// If the state is indicating by changeState no longer equalling NO_CHANGE,
	// it means it wants to transition. So do so.
	if (changeState != SlimeState::NO_CHANGE)
	{
		this->ChangeState(changeState,
						  a_p_entities,
						  a_p_ownerAgent,
						  a_p_worldGraph);
	}
}

// Change the state. Swicthes between two different states.
void StateMachine::ChangeState(const SlimeState a_newState,
							   EntityManager *a_p_entities,
							   Agent *a_p_ownerAgent,
							   Graph *a_p_worldGraph)
{
	// Exit from the current state.
	m_p_currentState->Exit();

	// Set the previous state to the current state
	delete m_p_previousState;
	m_p_previousState = m_p_currentState;

	// Set the current state to the relevant state.
	switch (a_newState)
	{
	case FIND:  // Set the find state
		m_p_currentState = new FindState();
		break;
	case SLOW_FIND:  // Set the slow find state.
		m_p_currentState = new SlowFindState();
		break;
	case PURSUE:  // Set the pursue state.
		m_p_currentState = new PursueState();
		break;
	case EVADE:  // Set the evade state.
		m_p_currentState = new EvadeState();
		break;
	default:
		break;
	}

	// Initialise the new state
	m_p_currentState->Init(
		a_p_entities,
		a_p_ownerAgent,
		a_p_worldGraph);
}
