/*-----------------------------------------------------------------------------
file:			Sequence.cpp
author:			Brad Cook
description:	This cpp file contains the definition of the Sequence
				class: The class for the sequence node behaviour of a behaviour
				tree.
				Source: http://www.aigamedev.com
-----------------------------------------------------------------------------*/

#include "Sequence.h"

Sequence::~Sequence()
{
	clearChildren();
}

void Sequence::onInitialize()
{
	m_current = m_children.begin();
}

Status Sequence::update(EntityManager *a_p_entities,
	Blobby *a_p_ownerAgent,
	Graph *a_p_worldGraph,
	float deltaTime)
{
	// Keep going until a child behavior says it's running.
    while(true)
    {
		Status s = (*m_current)->tick(a_p_entities,
			a_p_ownerAgent,
			a_p_worldGraph,
			deltaTime);

        // If the child fails, or keeps running, do the same.
        if (s != BH_SUCCESS)
        {
            return s;
        }

        // Hit the end of the array, job done!
        if (++m_current == m_children.end())
        {
            return BH_SUCCESS;
        }
    }
}