/*-----------------------------------------------------------------------------
file:			Behaviour.h
author:			Brad Cook
description:	This header file contains the declaration of the Behaviour
				class: The base class for all behaviours of a behaviour tree.
				Source: http://www.aigamedev.com
-----------------------------------------------------------------------------*/

#pragma once

enum Status
{
	BH_INVALID,
	BH_SUCCESS,
	BH_FAILURE,
	BH_RUNNING,
	BH_ABORTED
};

// Forward Declares
class EntityManager;
class Blobby;
class Graph;


// Base class for actions, conditions and composites
class Behaviour
{
public:
	Behaviour() : m_eStatus(BH_INVALID) {}
	virtual ~Behaviour() {}

public:
	virtual Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime) = 0;
	virtual void onInitialize() {}
	virtual void onTerminate(Status status) {}

	Status tick(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
	void reset();
	void abort();
	
	bool isTerminated() const;
	bool isRunning() const;

	Status getStatus() const;

private:
	Status m_eStatus;
};

