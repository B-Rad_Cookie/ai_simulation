/*-----------------------------------------------------------------------------
file:			ObjAvoid.h
author:			Brad Cook
description:	This header file contains the declaration of the ObjAvoid
				class: The class which implements the object collision
				avoidance AI Steering behaviour.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class EntityManager;
class Polygon;
class Circle;

// Includes
#include "Agent.h"
#include <glm\vec2.hpp>

// The class for the "Object Avoidance" steering behaviour. Calculates the
// force required to avoid game world obstacles.
class ObjAvoid
{
public:
	/*-------------------------------------------------------------------------
							Static Public Methods
	-------------------------------------------------------------------------*/
	// Returns the glm::vec2 force to apply to an agent such that the agent
	// moves away from the closest obstacle.
	static glm::vec2 CalculateForce(const MovementInfo &a_agent,
		const EntityManager *a_entities);

	// Determines whether the polygon a_p_poly is closer the the agent than the
	// circle a_p_circle. Returns true if the poly is closer, returns false if
	// the circle is closer.
	static bool IsPolyCloser(const MovementInfo &a_agent,
							 const Polygon *a_p_poly,
							 const Circle*a_p_circle);

	// Returns the avoidance force required for avoiding an obstacle.
	static glm::vec2 GetForce(const MovementInfo &a_agent,
		const Circle* a_p_circle);

	// Returns the avoidance force required for avoiding an obstacle.
	static glm::vec2 GetForce(const MovementInfo &a_agent,
		const Polygon* a_p_poly);

	/*-------------------------------------------------------------------------
						Static Public Member Variables
	-------------------------------------------------------------------------*/
	// The scalar avoidance force to apply
	static const float AVOIDANCE_FORCE;
};
