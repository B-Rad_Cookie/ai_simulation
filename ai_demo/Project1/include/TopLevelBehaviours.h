/*-----------------------------------------------------------------------------
file:			TopLevelBehaviours.h
author:			Brad Cook
description:	This header file contains the declarations of classes (nodes)
				that are at the high end (close to the root) of the behaviour
				tree.
-----------------------------------------------------------------------------*/

#pragma once

#include "Behaviour.h"
#include "Decorator.h"

// Class which sets Blobby's powered up state.
class PowerUpBehaviour : public Behaviour
{
public:
	// Sets the powerup state of Blobby.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines whether Blobby is powered up or not. Returns Success
// if she is, returns failure if she isn't.
class IsPoweredBehaviour : public Behaviour
{
public:
	// Determines whether Blobby is powered up
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines the max speed Blobby should travel at, based on
// whether she needs to evade.
class SetSpeedBehaviour : public Behaviour
{
public:
	// Determines whether Blobby needs to evade, and set her max speed
	// accordingly.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which switches the return status for a child behaviour.
class Inverter : public Decorator
{
public:
	// Swicthes the status state.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which always returns true.
class Successor : public Decorator
{
public:
	// Always returns success as the status.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};
