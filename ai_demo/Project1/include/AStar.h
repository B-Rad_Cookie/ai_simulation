/*-----------------------------------------------------------------------------
file:			AStar.h
author:			Brad Cook
date modified:	29/08/2015
description:	This header file contains the declaration of the AStar class:
				Finds the shortest path between two nodes on a grpah using the
				A* algorithm.
-----------------------------------------------------------------------------*/

#pragma once

// Includes
#include "Graph.h"
#include <vector>

// Runs the  A* algorithm on a graph to get the shortest distance between two
// nodes on the graph. This class works for both directed and non-directed
// graphs. The shortest path is returned by the algorithm as a std::vector of
// pointers to the nodes which make up the shortest path. Therefore, you need
// to set up a std::vector of type Graph node pointer to pass in.
class AStar
{
public:
	// Represents an A* pathfinding node used in the algorithm.
	struct AStarNode
	{
		// Empty constructor. Not that the f score is set to INFINITY. This is
		// so the algorithm can work correctly.
		AStarNode() :
			m_p_graphNode(nullptr), m_p_parentPathNode(nullptr),
			gScore(0.0f),
			hScore(0.0f),
			fScore(INFINITY){}
		// Original Graph Node constructor. Not that the f score is set to
		// INFINITY. This is so the algorithm can work correctly.
		AStarNode(Graph::Node* pOriginalNode) :
			m_p_graphNode(pOriginalNode), m_p_parentPathNode(nullptr),
			gScore(0.0f),
			hScore(0.0f),
			fScore(INFINITY){}

		/*-----------------------
		Public member variables
		-----------------------*/
		// The original graph
		Graph::Node *m_p_graphNode;

		// Information required for Pathfinding.
		// The pathfinding node which a pathnode came from to get to this path
		// finding node.
		AStarNode *m_p_parentPathNode;
		// The g-score. The accumulative cost of traversing over the edges.
		float gScore;
		// The h-score. The score calculate for the A* heuristic.
		float hScore;
		// The f-score. The combination of the g and h scores. Used to
		// determine the path.
		float fScore;
	};

	// Empty Constructor
	AStar();
	// Destructor
	~AStar();

	/*--------------
	Public Methods
	--------------*/
	// Find the shortest path between two nodes using the A* algorithm. a_Graph
	// is the graph, a_p_start and a_p_end are pointers to the start and end
	// nodes to find a path between respectively, and a_shortestPath is the
	// std::vector of Graph Node pointers which contains the shortest path.
	void AStarShortestPath(
		Graph &a_Graph,
		Graph::Node *a_p_start,
		Graph::Node *a_p_end,
		std::vector<Graph::Node*> &a_shortestPath);
private:
	/*---------------
	Private Methods
	---------------*/
	// Remove all created Pathfinding Nodes
	void Destroy();

	/*------------------------
	Private member variables
	------------------------*/
	// A record of all Pathfinding Nodes created
	std::vector<AStarNode*> m_PathNodes;	
};

// A Predictae functor used for sorting the priority queue used in the A*
// algorithm.
class SortAStarNodeAscending
{
public:
	bool operator()(
		const AStar::AStarNode* a_p_lhs,
		const AStar::AStarNode* a_p_rhs);
};
