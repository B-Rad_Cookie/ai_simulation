/*-----------------------------------------------------------------------------
file:			EntityManager.h
author:			Brad Cook
description:	This header file contains the declaration of the EntityManager
				class: The class which contains and controls all game objects.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class BlackSlime;
class Polygon;
class Circle;
class Well;
class Graph;
class Texture;
class SpriteBatch;
class Timer;

// Includes
#include "Blobby.h"
#include <glm\vec2.hpp>
#include <vector>

// Stores and controls all the entities in the game world. It also controls all
// the drawing of the game world objects.
class EntityManager
{
public:
	// Default Constructor
	EntityManager();

	// Destructor
	~EntityManager();

	/*-------------------------------------------------------------------------
								Public Methods
	-------------------------------------------------------------------------*/

	// The initial position for the game world entities.
	void Init();

	// Create the Game agents.
	void InitAgents(Graph *a_p_worldGraph);

	// Removes the game world components.
	void Destroy();

	// Removes the game world agents.
	void DestroyAgents();

	// Updates all the required entities.
	void Update(Graph *a_p_worldGraph, float dT);

	// Draws all the entities.
	void Draw(SpriteBatch *a_p_spritebatch);

	// Returns a const of Blobby
	const Blobby* GetBlobby() const { return m_p_blobby; } 

	// Returns a const of the BlackSlimes
	const std::vector<BlackSlime*> GetSlimes() const { return m_slimes; }

	// Returns a const of the wells
	const std::vector<Well*> GetWells() const { return m_wells; }

	// Returns a const of the water
	const Polygon* GetWater() const { return m_p_water; }

	// Returns a const of the obstacles
	const std::vector<Circle*> GetObstacles() const { return m_obstacles; }

	// Returns a const of Paved areas
	const std::vector<Polygon*> GetPaved() const { return m_pavedAreas; }

	// Returns a const of Paved areas
	const std::vector<Polygon*> GetRock() const { return m_rockAreas; }

	// Returns a const of the Walls
	const std::vector<Polygon*> GetWalls() const { return m_sidewalls; }

	// Returns a const of the Crate
	const Polygon* GetCrate() const { return m_p_crate; }

	// Removes the Crate from the game world.
	void RemoveCrate();

	// Removes the given Slime.
	void RemoveSlime(BlackSlime* a_p_slime);

	// Returns the crate spawn points.
	const glm::vec2& GetLastCrateSpawnPoint()
	{ return m_crateSpawnSpots[m_crateSpawnIndex]; }

	// Creates a Black Slime at one of the wells.
	void CreateBlackSlime(Graph* a_p_worldGraph);

	// Creates a crate at a random location.
	void CreateCrate();

	/*-------------------------------------------------------------------------
								Private Members
	-------------------------------------------------------------------------*/
private:
	// Blobby
	Blobby *m_p_blobby;

	// The Black Slimes.
	std::vector<BlackSlime*> m_slimes;

	// The walls. The first item is the left wall, second item is the right
	// wall, third item is the top wall, and last item is the bottom wall. 
	std::vector<Polygon*> m_sidewalls;

	// The circular obstacles.
	std::vector<Circle*> m_obstacles;

	// The wells for which black slimes appear and can disappear.
	std::vector <Well*> m_wells;

	// The water hazard in the middle of the simulation.
	Polygon *m_p_water;

	// The paved areas.
	std::vector<Polygon*> m_pavedAreas;

	// The rocky areas.
	std::vector<Polygon*> m_rockAreas;

	// The Crate
	Polygon *m_p_crate;

	// The sprite texture for Blobby.
	Texture *m_p_blobbyT;

	// The sprite texture for Black Slimes.
	Texture *m_p_slimeT;

	// The sprite texture for Open Wells.
	Texture *m_p_openWellT;

	// The sprite texture for Closed Wells.
	Texture *m_p_closedWellT;

	// The sprite texture for Obstacles.
	Texture *m_p_obstacleT;

	// The sprite texture for Rocky Areas.
	Texture *m_p_rockT;

	// The sprite texture for Paved Areas.
	Texture *m_p_paveT;

	// The sprite texture for Vertical Walls.
	Texture *m_p_vertWallsT;

	// The sprite texture for Horizontal Walls.
	Texture *m_p_horizWallsT;

	// The sprite texture for Water.
	Texture *m_p_waterT;

	// The sprite texture for the Crate.
	Texture *m_p_crateT;

	// Sprite for the line of sights.
	Texture *m_p_sightT;

	// Timer for the Black Slime respawn.
	Timer *m_p_slimeRespawnTimer;

	// Timer for Crate respawn.
	Timer *m_p_crateRespawnTimer;

	// The possible locations a crate can respawn in.
	std::vector<glm::vec2> m_crateSpawnSpots;

	// The index of the crate spawn position where a crate was spawned.
	int m_crateSpawnIndex;
};
