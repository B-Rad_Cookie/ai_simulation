/*-----------------------------------------------------------------------------
file:			SteerBehaviours.h
author:			Brad Cook
description:	This header file contains the declarations of classes (nodes)
				used to perform steering behaviours (evade and pursue) as part
				of a Behaviour Tree. Note: These are NOT the actual steering
				themselves. See Pursue.h, Evade.h and Seek.h for those.
-----------------------------------------------------------------------------*/

#pragma once
#include "Behaviour.h"

// Class which determines whether a slime is in sight to pursue.
class InSightBehaviour : public Behaviour
{
public:
	// Determines whether a black slime is in sight. Returns success if there
	// is, returns failure otherwise.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};
