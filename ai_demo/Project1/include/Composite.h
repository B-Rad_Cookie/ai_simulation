/*-----------------------------------------------------------------------------
file:			Composite.h
author:			Brad Cook
description:	This header file contains the declaration of the Composite
				class: The base class for composite (collections of) behaviours
				of a behaviour tree.
				Source: http://www.aigamedev.com
-----------------------------------------------------------------------------*/

#pragma once
#include "Behaviour.h"
#include <vector>

class Composite : public Behaviour
{
public:
	~Composite();
	void addChild(Behaviour* child);
	void removeChild(Behaviour* child);
	void clearChildren();

protected:
	typedef std::vector<Behaviour*> Behaviours;
	Behaviours m_children;
};

