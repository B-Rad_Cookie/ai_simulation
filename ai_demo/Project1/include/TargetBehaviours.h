/*-----------------------------------------------------------------------------
file:			TargetBehaviours.h
author:			Brad Cook
description:	This header file contains the declarations of classes (nodes)
				used to perform setting pathfinding targets behaviours as part
				of a Behaviour Tree.
-----------------------------------------------------------------------------*/

#pragma once
#include "Behaviour.h"

// Class which determines whether a crate is currently on the world map.
class CrateAvailableBehaviour : public Behaviour
{
public:
	// Determines whether a crate is available. Returns success if it is,
	// returns failure otherwise.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines sets the crate as the pathfinding target.
class TargetCrateBehaviour : public Behaviour
{
public:
	// Sets the crate as the target
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines whether Blobby is evading.
class IsEvadeBehaviour : public Behaviour
{
public:
	// Evading is determined by whether the max speed is set to the max of 2.5f.
	// Returns success if she is, returns failure otherwise.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines whether Blobby should keep on following along a wall.
class FollowWallBehaviour : public Behaviour
{
public:
	// Checks whether Blobby is within a distance of any of the walls.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines whether Blobby is aiming straight for an outer wall.
class AimAtWallBehaviour : public Behaviour
{
public:
	// Checks whether Blobby, while evading, has a wall in sight.
	// Returns success if she does, returns failure otherwise.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines what end of the wall Blobby should flee to.
class TargetEndWallBehaviour : public Behaviour
{
public:
	// Determines which end of the wall Blobby should target. It does this by
	// targeting the end that the closest chasing Black Slimes is further away
	// from.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which determines whether a new target needs to be set away from the
// chasing slimes.
class NewEvadeTargetBehaviour : public Behaviour
{
public:
	// Determines whether a new target away from slimes needs to be set.
	// Returns success if it is, returns failure otherwise.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which sets the evade in opposite direction.
class SetEvadeTargetBehaviour : public Behaviour
{
public:
	// Sets the pathfinding target as 150 pixels away from all of the
	// black slimes.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// Class which sets the target as the last known crate location.
class SetLastCrateBehaviour : public Behaviour
{
public:
	// Sets the pathfinding target as 200 pixels in the direction of all the
	// black slimes.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

