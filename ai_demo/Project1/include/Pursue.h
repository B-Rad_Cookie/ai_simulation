/*-----------------------------------------------------------------------------
file:			Pursue.h
author:			Brad Cook
description:	This header file contains the declaration of the Pursue class:
				The class which implements the Pursue AI Steering behaviour.
-----------------------------------------------------------------------------*/

#pragma once

// Includes
#include "Agent.h"
#include <glm\vec2.hpp>

//The class for the "Pursue" steering behaviour. Calculates the force required
// to reach a target.
class Pursue
{
public:
	// Returns the glm::vec2 force to apply to an agent such that the agent
	// moves to the target agent.
	static glm::vec2 CalculateForce(const MovementInfo &a_agent,
									const MovementInfo &a_target);
};
