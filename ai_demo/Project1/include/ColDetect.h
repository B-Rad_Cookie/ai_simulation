/*-----------------------------------------------------------------------------
file:			ColDetect.h
author:			Brad Cook
description:	This header file contains the declaration of the ColDetect
				class: The utility class which handles collision detection of
				geometry objects.
-----------------------------------------------------------------------------*/

#pragma once

// Forward declares
class Circle;
class Polygon;

// Includes
#include <glm\vec2.hpp>
#include <vector>

// The Collision Detection utility class. Provides methods which determines
// whether geometries have collided.
class ColDetect
{
public:
	// Default constructor. As this is a utility class, does nothing.
	ColDetect() {};
	// Destructor. As this is a utility class, does nothing.
	~ColDetect() {};

	// Determines whether two circles are intersecting.
	static bool AreColliding(const Circle* a_p_c1, const Circle* a_p_c2);

	// Determines whether a point falls within a circle.
	static bool AreColliding(const glm::vec2 &a_point,
							 const Circle* a_p_circle);

	// Determines whether a segment intersects a circle.
	static bool AreColliding(const glm::vec2 &a_seg_start,
							 const glm::vec2 &a_seg_end,
							 const Circle* a_p_circle);

	// Determines whether a point falls within a Polygon.
	static bool AreColliding(const glm::vec2 &a_point,
							 const Polygon* a_p_poly);

	// Determines whether a segment falls within a polygon.
	static bool AreColliding(const glm::vec2 &a_seg_start,
							 const glm::vec2 &a_seg_end,
							 const Polygon* a_p_polygon);

	// Determines whether a circle falls within a polygon.
	static bool AreColliding(const Circle* a_p_circle,
							 const Polygon* a_p_polygon);

	// Determines whether a polygon falls within a polygon.
	static bool AreColliding(const Polygon* a_p_poly1,
							 const Polygon* a_p_poly2);

	// Determines whether two shapes, specified as indices, collide using the
	// Separate Axis Thereom. reutnrs true if they do, returns false otherwise.
	static bool SeparateAxisThereom(
		const std::vector<glm::vec2> &a_axesOfSeparation,
		const std::vector<glm::vec2> &a_shapeOneIndices,
		const std::vector<glm::vec2> &a_shapeTwoIndices);

	// Adds the axis from the indices to the axes of separation.
	static void GetAxesOfSeparation(std::vector<glm::vec2> &a_AOS,
									const std::vector<glm::vec2> &a_indices);
};

