
/*-----------------------------------------------------------------------------
Author:			
Description:	
-----------------------------------------------------------------------------*/
#ifndef GAME1_H
#define GAME1_H

// Forward declarations
class SpriteBatch;
class Texture;
class Polygon;
class EntityManager;

// Includes
#include "Application.h"
#include "Graph.h"

class Game1 : public Application
{
public:

	// assets loaded in constructor
	Game1(unsigned int windowWidth, unsigned int windowHeight, bool fullscreen, const char *title);

	// assets destroyed in destructor
	virtual ~Game1();

	// update / draw called each frame automaticly
	virtual void Update(float deltaTime);
	virtual void Draw();

protected:
	// The Spritebatch drawing object
	SpriteBatch *m_spritebatch;
	// The underlying graph to use for the simulation.
	Graph *m_p_graph;

private:

	/*-------------------------------------------------------------------------
								Private methods
	-------------------------------------------------------------------------*/
	// Creates the grid to be used for pathfinding purposes.
	void BuildGraphGrid();

	// Connects a potential node at the X,Y co-ordinate a_xcoord, a_ycoord to
	// the node a_p_newNode in the world graph with the edge weight muplied by
	// a_weightMultiply.
	void Game1::ConnectNewNode(
		Graph::Node *a_p_newNode,
		int a_xcoord,
		int a_ycoord,
		float a_weightMultiply,
		const std::vector<Polygon*> &a_rockAreas,
		const std::vector<Polygon*> &a_paveAreas);
	
	/*-------------------------------------------------------------------------
							Private member variables
	-------------------------------------------------------------------------*/

	// Background texture
	Texture *m_p_backgroundTexture;

	// The manager for all the game world entities.
	EntityManager *m_p_entityManager;
};

#endif
