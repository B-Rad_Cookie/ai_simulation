/*-----------------------------------------------------------------------------
file:			Evade.h
author:			Brad Cook
description:	This header file contains the declaration of the Evade class:
				The class which implements the Evade AI Steering behaviour.
-----------------------------------------------------------------------------*/

#pragma once

// Includes
#include "Agent.h"
#include <glm\vec2.hpp>

//The class for the "Evades" steering behaviour. Calculates the force required
// to move away from a target.
class Evade
{
public:
	// Returns the glm::vec2 force to apply to an agent such that the agent
	// moves to the target agent.
	static glm::vec2 CalculateForce(const MovementInfo &a_agent,
									const MovementInfo &a_target);
};
