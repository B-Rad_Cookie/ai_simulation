/*-----------------------------------------------------------------------------
file:			States.h
author:			Brad Cook
description:	This header file contains the declarations of all the states
				for the black slime state machine.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class Blobby;
class Agent;
class EntityManager;
class Polygon;

// Includes
#include "IState.h"
#include "Graph.h"
#include <vector>

// The Base for the Find States. As the two find states share a lot in common,
// the common tasks will be handled by this class instead, with the two actual
// state classes subclassing this one.
class BaseFindState : public IState
{
public:
	/*-------------------------------------------------------------------------
									PUBLIC METHODS
	-------------------------------------------------------------------------*/

	// Adds the force to a black slime which is required to get to Blobby. This
	// done by finding the shortest path to Blobby (using the A* algorithm),
	// then following the path to Blobby. The a_changeState is used to indicate
	// back to the StateMachine what state the state machine should switch to.
	virtual void Update(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		SlimeState &a_changeState,
		float deltaTime);

	// Initialises the state. Set the shortest path to Blobby.
	virtual void Init(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph);

	// Exit function. Cleans up all the memory used.
	void Exit();

protected:
	/*-------------------------------------------------------------------------
							    PROTECTED METHODS
	-------------------------------------------------------------------------*/
	// Sets the shortest path from the black slime to the target vector.
	void SetShortestPath(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph);

	/*-------------------------------------------------------------------------
							PROTECTED MEMBER VARIABLES
	-------------------------------------------------------------------------*/
	// Reference to the target.
	glm::vec2 m_targetVector;

	// The shortest path.
	std::vector<Graph::Node*> m_shortestPath;

	// The index of the node in the shortestPath vector to seek
	int m_currentNodeIndex;

	// The node closest to the shortest path target, Blobby. This info is kept
	// so that the shortest path algorithm isn't called every frame: only when
	// Blobby has moved.
	Graph::Node *m_p_target;
};

// The Find state for the black slime. This state is where tthe black slime
// moves towards Blobby.
class FindState : public BaseFindState
{
public:
	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	// Adds the force to a black slime which is required to get to Blobby. This
	// done by finding the shortest path to Blobby (using the A* algorithm),
	// then following the path to Blobby. The a_changeState is used to indicate
	// back to the StateMachine what state the state machine should switch to.
	void Update(EntityManager *a_p_entities,
				Agent *a_p_ownerAgent,
				Graph *a_p_worldGraph,
				SlimeState &a_changeState,
				float deltaTime);

	// Initialises the state. Find the shortest path to Blobby, then set the
	// second node in the graph as the target to go to.
	void Init(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph);

	// Exit function. Cleans up all the memory used.
	void Exit();

private:
	/*-------------------------------------------------------------------------
						PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/
	const Blobby* m_p_blobby;
};

// The Slow Find state for the black slime. This state is where the black slime
// moves towards Blobby, like the find state, but does so more slowly, as there
// is a well or crate nearby.
class SlowFindState : public BaseFindState
{
public:
	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	// Adds the force to a black slime which is required to get to Blobby. This
	// done by finding the shortest path to Blobby (using the A* algorithm),
	// then following the path to Blobby. The a_changeState is used to indicate
	// back to the StateMachine what state the state machine should switch to.
	void Update(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		SlimeState &a_changeState,
		float deltaTime);

	// Initialises the state. Find the shortest path to Blobby, then set the
	// second node in the graph as the target to go to.
	void Init(EntityManager *a_p_entities,
			  Agent *a_p_ownerAgent,
			  Graph *a_p_worldGraph);

	// Exit function. Cleans up all the memory used.
	void Exit();

private:
	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/
	// Blobby
	const Blobby* m_p_blobby;
};

// The Pursue state for the black slime. This state is where the black slime
// chases after Blobby when she is spotted.
class PursueState : public BaseFindState
{
public:
	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	// Adds the force to a black slime which is required to pursue Blobby. The
	// a_changeState is used to indicate back to the StateMachine what state
	// the state machine should switch to.
	void Update(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		SlimeState &a_changeState,
		float deltaTime);

	// Initialises the state. Sets Blobby as the target.
	void Init(EntityManager *a_p_entities,
			  Agent *a_p_ownerAgent,
			  Graph *a_p_worldGraph);

	// Exit function. Cleans up all the memory used.
	void Exit();

private:
	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/
	// Blobby.
	const Blobby* m_p_blobby;
};

// The Evade state for the black slime. This state is where the black slime
// runs from Blobby when she is spotted and powered up.
class EvadeState : public BaseFindState
{
public:
	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	// Adds the force to a black slime which is required to pursue Blobby. The
	// a_changeState is used to indicate back to the StateMachine what state
	// the state machine should switch to.
	void Update(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		SlimeState &a_changeState,
		float deltaTime);

	// Initialises the state. Sets Blobby as the target.
	void Init(EntityManager *a_p_entities,
			  Agent *a_p_ownerAgent,
			  Graph *a_p_worldGraph);

	// Exit function. Cleans up all the memory used.
	void Exit();

private:
	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/
	// Determines whether the slime is aiming at a wall.
	bool IsAimAtWall(EntityManager *a_p_entities,
					 Agent *a_p_ownerAgent);

	// Determines whether the slime within a distance of a wall, and therefore
	// traversing alongside it.
	bool IsAlongWall(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent);

	// Sets a side of the wall that the slime is facing as the pathifinding
	// target.
	void TargetWallEnd(Agent *a_p_ownerAgent);

	// Sets a position 200 pixels away from Blobby as the evade target.
	void SetEvade(Agent *a_p_ownerAgent);

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/
	// The wall the slime is colliding with.
	Polygon *m_p_aheadWall;

	// Blobby
	const Blobby* m_p_blobby;
};
