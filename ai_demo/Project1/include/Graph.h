/*-----------------------------------------------------------------------------
file:			Graph.h
author:			Brad Cook
date modified:	28/08/2015
description:	This header file contains the declaration of the Graph class:
				which models a graph with nodes and edges. It also contains
				defintions of functors used for graph operations.
-----------------------------------------------------------------------------*/

#pragma once

// Includes
#include <vector>

// Represents a graph with nodes and edges. This graph representation is done
// using a node list and an edge list.
// The graph can either be directed or non-directed. By default, graphs are
// non-directed. To make a graph directed, simply use the Is Directed
// constructor and pass in true.
class Graph
{
public:
	// Represents a node in the graph. The node contains an x,y vector2
	// position.
	class Node
	{
	public:
		// Empty Constructor
		Node();
		// Copy Constructor;
		Node(const Node &a_otherNode);
		// X,Y float co-ordinate
		Node(const int a_x, const int a_y);
		// Destructor
		~Node();

		/*--------------
		Public methods
		--------------*/
		// Equivalency opertor overload. Tests whether two nodes are the same.
		bool operator==(const Node &a_otherNode);

		// Non-Equivalency opertor overload. Tests whether two nodes are the
		// same.
		bool operator!=(const Node &a_otherNode);

		/*-----------------------
		Public member variables
		-----------------------*/
		// The x and y co-ordinate.
		int m_x;
		int m_y;
	};

	// Represents an edge in the graph. An edge contains a pointer to the start
	// and end Graph::Nodes, as well as a weight value.
	class Edge
	{
	public:
		// The start node of the edge
		Node* m_p_startNode;
		// The end node of the edge.
		Node* m_p_endNode;
		// The weight (i.e. cost) of travelling over the edge.
		float m_weight;
	};

public:
	// Empty Constructor
	Graph();
	// Is Directed Constructor 
	Graph(const bool a_isDirected);
	// Destructor.
	~Graph();

	/*--------------
	Public Methods
	--------------*/
	// Frees up the memory used by the Graph instance.
	void Destroy();

	// Determines whether the graph is bi-directional or not.
	bool IsDirected() const;
	
	// Adds a Node at the co-ordinates a_xcoord and a_ycoord. Returns the
	// pointer to the created Node object.
	Node* AddNode(int a_xcoord, int a_ycoord);

	// Finds and returns the node at the X, Y co-ordinate position a_xcoord and
	// a_ycoord. Returns nullptr if it can not be found.
	Graph::Node* FindNode(int a_xcoord, int a_ycoord) const;

	// Finds and returns the node nearest to the X, Y co-ordinate position
	// a_xcoord and a_ycoord.
	Graph::Node* FindNearestNode(
		float a_xcoord, float a_ycoord) const;

	// Remove the given node a_p_Node
	void RemoveNode(Node *a_p_Node);

	// Connects the node a_p_NodeStart to a_p_NodeEnd with an edge with a weight
	// (cost) of a_weight.
	void ConnectNodes(Node *a_p_NodeStart, Node *a_p_NodeEnd, float a_weight);

	// Returns a std::vector of the edges that a_p_Node connects to. It will
	// return all the connections based on whether the graph is directed or
	// not. i.e. if your graph is directed, it will only return edges where the
	// node is the start node. If no-directed, it will return all edges.
	std::vector<Graph::Edge*> FindAllConnections(Node* a_p_Node);

private:
	/*---------------
	Private Methods
	---------------*/
	// Returns a std::vector of every edge that a_p_Node is part of. Used to find
	// connections in Non-directed graphs.
	void Graph::FindNonDirectedConnections(
		Node* a_p_Node,
		std::vector<Graph::Edge*> &a_edgeConnections);

	// Returns a std::vector of every edge where a_p_Node is the start node. Used
	// to find edges in directed graphs.
	void Graph::FindDirectionalConnections(
		Node* a_p_Node,
		std::vector<Graph::Edge*> &a_edgeConnections);

	/*------------------------
	Private member variables
	------------------------*/
	// The Node list.
	std::vector<Node*> m_lNodes;
	// The Edge list
	std::vector<Edge*> m_lEdges;
	// Whether or not the Graph is bi-directional.
	bool m_isDirected;
};

// Functor for use with the remove-erase idiom for removing nodes from the edge
// list.
class NodeInEdge
{
private:
	/*------------------------
	Private member variables
	------------------------*/
	// The Node to be removed from the graph.
	Graph::Node *m_p_removeNode;
public:
	// Empty Constructor
	NodeInEdge() : m_p_removeNode(nullptr){}
	// Set remove node constructor.
	NodeInEdge(Graph::Node *a_Node) : m_p_removeNode(a_Node){}
	
	/*--------------
	Public Methods
	--------------*/
	// () operator overload. Returns true if a_p_edge contains the node to be
	// removed.
	bool operator() (const Graph::Edge *a_p_edge);
};

// Functor for use with the remove-erase idiom for finding graph edges where a
// node is not the start node of the edge connection.
class NodeIsEndNode
{
private:
	/*------------------------
	Private member variables
	------------------------*/
	// The node
	Graph::Node *m_p_node;
public:
	// Empty constructor
	NodeIsEndNode() : m_p_node(nullptr){}
	// Set node constructor
	NodeIsEndNode(Graph::Node *a_pNode) : m_p_node(a_pNode){}

	/*--------------
	Public Methods
	--------------*/
	// () operator overload. Returns true if the node is the end node of the edge
	// a_p_edge
	bool operator() (const Graph::Edge *a_p_edge);
};