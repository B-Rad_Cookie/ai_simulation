/*-----------------------------------------------------------------------------
file:			Polygon.h
author:			Brad Cook
description:	This header file contains the declaration of the Polygon
				class: The class which represents a polygon geometry
-----------------------------------------------------------------------------*/

#pragma once

// Includes
#include <glm\vec2.hpp>
#include <vector>

// A class representing a polygon geometry. The polygon is defined as a list of
// the vertices.
class Polygon
{
public:
	// Indices Constructor. Create a new Polygon with the listed indices and
	// central point. The indices must be specified in a clockwise-direction.
	Polygon(const std::vector<glm::vec2> &a_indices,
			const glm::vec2 &a_centre);

	/*-------------------------------------------------------------------------
							Public member variables
	-------------------------------------------------------------------------*/
	// The vertices of the polygon.
	std::vector<glm::vec2> m_indices;

	// The centre of the polygon.
	glm::vec2 m_centre;

	// Finds the distance from the polygon to a point.
	float DistanceToPoint(const glm::vec2 &a_point) const;
};
