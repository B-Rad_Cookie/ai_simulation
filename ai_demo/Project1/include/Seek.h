/*-----------------------------------------------------------------------------
file:			Seek.h
author:			Brad Cook
description:	This header file contains the declaration of the Seek class:
				The class which implements the Seek AI Steering behaviour.
-----------------------------------------------------------------------------*/

#pragma once

// Includes
#include "Agent.h"
#include <glm\vec2.hpp>

//The class for the "Seek" steering behaviour. Calculates the force required
// to reach a target.
class Seek
{
	/*-------------------------------------------------------------------------
							Public Static Methods
	-------------------------------------------------------------------------*/
public:
	// Returns the glm::vec2 force to apply to an agent such that the agent
	// moves to the target agent.
	static glm::vec2 CalculateForce(const MovementInfo &a_agent,
									const MovementInfo &a_target);

	// Returns the glm::vec2 force to apply to an agent such that the agent
	// moves to the target vector.
	static glm::vec2 CalculateForce(const MovementInfo &a_agent,
									const glm::vec2 &a_target);
};
