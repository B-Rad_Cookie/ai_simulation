/*-----------------------------------------------------------------------------
file:			LureBehaviours.h
author:			Brad Cook
description:	This header file contains the declarations of classes (nodes)
				used to perform luring slimes into a well behaviour as part of
				a Behaviour Tree.
-----------------------------------------------------------------------------*/

#pragma once

#include "Behaviour.h"

// Class which determines whether Blobby is near an open well.
class NearOpenWellBehvaiour : public Behaviour
{
public:
	// Determines whether Blobby is within 100 pixels of an open well. If so,
	// returns success, returns failure otherwise.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// The class which finds the black slime closest to Blobby.
class FindSlimeBehaviour : public Behaviour
{
public:
	// Finds the closest black slime. "Closest" is defined as the Euclidean
	// distance.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// The class which calculates the vector from the closest slime to the well.
class SlimeToWellBehaviour : public Behaviour
{
public:
	// Calculates the vector from the slime to the well. This info is used to
	// find the spot for Blobby to get to to lure the slime in.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// The class which gets the vector target for Blobby to aim to to lure in the
// closest slime.
class GetLureTargetBehaviour : public Behaviour
{
public:
	// Set the projected slime-to-well vector end as the target.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};
