/*-----------------------------------------------------------------------------
file:			Timer.h
author:			Brad Cook
description:	This header file contains the declaration of the Timer class:
				A class which keeps track of time.
-----------------------------------------------------------------------------*/

#pragma once
class Timer
{
public:
	/*-------------------------------------------------------------------------
								Public Methods
	-------------------------------------------------------------------------*/
	// Destructor. Does nothing.
	~Timer(){}

	// Set Time to elapse.
	Timer(float a_timeToElapse);

	// Determines whether the time has elapsed.
	bool TimeElapsed();

	// Elapses time by the given amount (in seconds).
	void AddTime(float a_time);

	// Resets the timer back to 0
	void Reset();
	
private:
	/*-------------------------------------------------------------------------
							Private Member Variables
	-------------------------------------------------------------------------*/

	// The time to have elapse (in seconds).
	float m_setTime;

	// The time which has passed.
	float m_timeElapsed;
};
