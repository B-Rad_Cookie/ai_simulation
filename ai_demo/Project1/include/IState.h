/*-----------------------------------------------------------------------------
file:			IState.h
author:			Brad Cook
description:	This header file contains the declaration of the IState class:
				The interface class for all states used in the Finite State
				Machine (FSM).
-----------------------------------------------------------------------------*/

#pragma once

// Forward declares
class EntityManager;
class Agent;
class Graph;

// Includes
#include "StateMachine.h"

// The base interface class for all states. All State classes should inherit
// from this interface.
class IState
{
public:
	// Adds a force to the owner agent. Performs other tasks required of the
	// state during the update. The a_changeState is used to indicate back to
	// the StateMachine what state the state machine should switch to.
	virtual void Update(EntityManager *a_p_entities,
						Agent *a_p_ownerAgent,
						Graph *a_p_worldGraph,
						SlimeState &a_changeState,
						float deltaTime) = 0;

	// Initialise Function. Called when a state is first transitioned to.
	virtual void Init(EntityManager *a_p_entities,
					  Agent *a_p_ownerAgent,
					  Graph *a_p_worldGraph) = 0;

	// Exit function. Called when a state is finishing up.
	virtual void Exit() = 0;
};
