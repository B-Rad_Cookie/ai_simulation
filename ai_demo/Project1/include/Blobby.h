/*-----------------------------------------------------------------------------
file:			Blobby.h
author:			Brad Cook
description:	This header file contains the declaration of the Blobby class:
				The class which represents the Blobby agent.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class EntityManager;
class Sequence;
class BlackSlime;
class Timer;
class Polygon;

// Includes
#include "Graph.h"
#include "Agent.h"

// Information that is used throughout the AI Behaviour Tree.
struct BehaviourTreeInfo
{
	// Constructor. Initialise everything to blank.
	BehaviourTreeInfo();

	// Destructor. Remove all memory created.
	~BehaviourTreeInfo();

	// Shortest Path to the Crate
	std::vector<Graph::Node*> m_shortestPath;

	// The node where the crate is.
	Graph::Node* m_p_targetNode;

	// The slime closest to Blobby to path find.
	glm::vec2 m_pathTargetVector;

	// The slime closest to Blobby.
	glm::vec2 m_slimeVector;

	// The vector from the closest slime to the open well.
	glm::vec2 m_slimeToWellVector;

	// Determines whether a new evade target needs to be set.
	bool m_newEvadeTarget;

	// Determines whether Blobby is running along the wall.
	bool m_alongWall;

	// Determines whether Blobby is setting the timer for evading.
	bool m_timing;

	// The wall which is ahead of Blobby while evading.
	Polygon* m_p_aheadWall;

	// The vector to the opened well.
	glm::vec2 m_openWell;

	// Index of the path to seek for path following.
	int m_pathIndex;

	// Amount of Time Blobby should continue to evade after escaping the view
	// of a slime.
	Timer *m_p_evadeTime;
};

// Represents the Blobby agent. Controls all the relevant information
// relating to a Blobby game entity.
class Blobby : public Agent
{
public:
	// Default Constructor
	Blobby();

	// Constructor with X, Y starting position co-ordinates
	Blobby(float a_positionX,
		   float a_positionY);

	// Constructor with Vector2 starting position.
	Blobby(const glm::vec2 &a_position);

	// Returns whether Blobby is powered up or not. Returns true if she is,
	// returns false otherwise.
	bool IsPoweredUp() const { return m_isPoweredUp; }

	// Sets the powered up state of Blobby
	void SetPowerUpState(bool a_isPoweredUp) { m_isPoweredUp = a_isPoweredUp; }

	//Destructor
	~Blobby();

	/*-------------------------------------------------------------------------
								Public Methods
	-------------------------------------------------------------------------*/

	// Update the BlackSlime information.
	void Update(EntityManager *a_p_entities,
		Graph *a_p_worldGraph,
		float deltaTime);

	/*-------------------------------------------------------------------------
							Public Member Variables
	-------------------------------------------------------------------------*/

	// The Behaviour tree information.
	BehaviourTreeInfo *m_p_treeInfo;

	// Powerup Timer
	Timer *m_p_powerUpTimer;

private:
	/*-------------------------------------------------------------------------
							Private Member Variables
	-------------------------------------------------------------------------*/
	// The root of the behaviour tree.
	Sequence *m_p_rootBehaviour;

	// Whether Blobby is powered up or not
	bool m_isPoweredUp;
};
