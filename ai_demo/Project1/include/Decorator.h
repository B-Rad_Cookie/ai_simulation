/*-----------------------------------------------------------------------------
file:			Decorator.h
author:			Brad Cook
description:	This header file contains the declaration of the Decorator
				class: The base class for decorator node behaviours of a
				behaviour tree.
-----------------------------------------------------------------------------*/

#pragma once
#include "Behaviour.h"
class Decorator : public Behaviour
{
public:
	Decorator();
	virtual ~Decorator();
	void addChild(Behaviour* child);
	void removeChild();

protected:
	Behaviour *m_p_child;
};
