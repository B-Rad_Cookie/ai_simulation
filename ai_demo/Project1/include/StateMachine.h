/*-----------------------------------------------------------------------------
file:			StateMachine.h
author:			Brad Cook
date modified:	07/10/2015
description:	This header file contains the declaration of the StateMachine
				class: This is the Finite State Machine (FSM) for the Black
				Slime agents.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class IState;
class EntityManager;
class Agent;
class Graph;

// Includes
#include <glm\vec2.hpp>

// An enumerator type for all the states of a Black Slime. NO_CHANGE is the
// value to use to specifiy to not change the state (it is not an actual
// state).
enum SlimeState
{
	NO_CHANGE,
	FIND,
	SLOW_FIND,
	PURSUE,
	EVADE
};

// The State Machine class which handles the states, as well as transitioning
// between them, of an agent. It's update function will return a force that is
// to be applied to the agent.
class StateMachine
{
public:
	// Constructor which Sets up a new StateMachine instance. The first state
	// is set to FIND.
	StateMachine(EntityManager *a_p_entities,
		Agent *a_p_ownerAgent,
		Graph *a_p_worldGraph);

	// Destrcutor
	~StateMachine();

	/*-------------------------------------------------------------------------
	                            Public Methods
	-------------------------------------------------------------------------*/
	// Applies the current state's force to the owner agent. It will calls the
	// current state's update function to calculate the force.
	void Update(EntityManager *a_p_entities,
				Agent *a_p_ownerAgent,
				Graph *a_p_worldGraph,
				float deltaTime);

	// Get Current state.
	const IState* GetCurrentState() { return m_p_currentState; }

	// Set Previous state.
	const IState* GetPreviousState() { return m_p_previousState; }

	// Change the state. Swicthes between two different states.
	void ChangeState(const SlimeState a_newState,
					 EntityManager *a_p_entities,
					 Agent *a_p_ownerAgent,
					 Graph *a_p_worldGraph);

private:
	/*-------------------------------------------------------------------------
	                      Private member variables
	-------------------------------------------------------------------------*/
	// The current state.
	IState* m_p_currentState;

	// The previous state.
	IState* m_p_previousState;
};
