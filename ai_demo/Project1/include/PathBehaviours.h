/*-----------------------------------------------------------------------------
file:			PathBehaviours.h
author:			Brad Cook
description:	This header file contains the declarations of classes (nodes)
				used to perform Pathfinding behaviours as part of a Behaviour
				Tree.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class EntityManager;
class Blobby;
class Graph;


#include "Behaviour.h"

// The class which finds the black slime closest to Blobby.
class ClosestSlimeBehaviour : public Behaviour
{
public:
	// Finds the closest black slime. "Closest" is defined as the Euclidean
	// distance.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// The class which determines whether a new shortest path needs to be
// calculated to get to the target.
class NeedNewPathBehaviour : public Behaviour
{
public:
	// Determines whether a new path is needed to get to the target.
	// If a new path is needed, success will be returned, otherwise failure
	// will be returned when not needed.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// The class which gets the path to the intended target for use with the path
// finding algorithm. 
class FindPathTargetBehaviour : public Behaviour
{
public:
	// Finds the the Path finding algorithm.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// The class which runs the path finding algorithm. 
class FindPathBehaviour : public Behaviour
{
public:
	// Runs the Path finding algorithm.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};

// The class which makes Blobby follow the path. 
class FollowPathBehaviour : public Behaviour
{
public:
	// Makes Blobby move along the path.
	Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);
};
