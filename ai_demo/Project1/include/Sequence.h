/*-----------------------------------------------------------------------------
file:			Sequence.h
author:			Brad Cook
description:	This header file contains the declaration of the Sequence
				class: The class for the sequence node behaviour of a behaviour
				tree.
				Source: http://www.aigamedev.com
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class EntityManager;
class Blobby;
class Graph;

// Includes
#include "Composite.h"

class Sequence : public Composite
{
public:
	virtual ~Sequence();
protected:	
	virtual void onInitialize();

	virtual Status update(EntityManager *a_p_entities,
		Blobby *a_p_ownerAgent,
		Graph *a_p_worldGraph,
		float deltaTime);

protected:
	Behaviours::iterator m_current;
};

