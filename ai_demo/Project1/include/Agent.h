/*-----------------------------------------------------------------------------
file:			Agent.h
author:			Brad Cook
date modified:	29/08/2015
description:	This header file contains the declaration of the Agent.h
				class: The base class for all artificial agents.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class EntityManager;
class Graph;
class Circle;

// Includes
#include <glm/vec2.hpp>
#include <vector>

// Struct containg the movement info of an agent.
struct MovementInfo
{
	// The location of the agent.
	glm::vec2 position;

	// The velocity of the agent.
	glm::vec2 velocity;

	// The acceleration of the agent.
	glm::vec2 acceleration;

	// The looking ahead (field of vision) for the agent.
	glm::vec2 ahead;

	// The max speed the agent can travel.
	float maxSpeed;
};

// Base class for artificial agents used in the simulation. It contains basic
// methods, as well as a couple interface methods, for artifical agents.
class Agent
{
public:
	// Empty Constructor
	Agent();

	// Constructor with X, Y starting position co-ordinates
	Agent(const float a_positionX,
		  const float a_positionY);

	// Constructor with Vector2 starting position.
	Agent(const glm::vec2 &a_position);

	//Destructor
	virtual ~Agent();

	/*-------------------------------------------------------------------------
									Public Methods
	-------------------------------------------------------------------------*/

	// Update the Agent information. To be implemented by subclasses.
	virtual void Update(EntityManager *a_p_entities,
						Graph *a_p_worldGraph,
						float deltaTime) = 0;

	/*-------------------------------------------------------------------------
									Public Variables
	-------------------------------------------------------------------------*/
	// Physics information for an agent.
	MovementInfo *m_p_agentMoveInfo;

	// The Bounding Circle for the agent. This is the shape used for
	// determining collision detetion of the agent with other agents or
	// objects. 
	Circle *m_p_boundCircle;

	// The line of sight for the agent.
	Circle *m_p_sight;
};
