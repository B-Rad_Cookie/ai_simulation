/*-----------------------------------------------------------------------------
file:			Circle.h
author:			Brad Cook
description:	This header file contains the declaration of the Circle class:
				The class which represents a circle geometry; and,  The Well
				class: The class which represents a well (a specialised
				Circle).
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class Timer;

// Includes
#include <glm\vec2.hpp>

// A class representing a circle.
class Circle
{
public:
	// Circle Constructor. Sets the circle to the given centre and radius.
	Circle(const glm::vec2 a_centre,
		   float a_radius);

	/*-------------------------------------------------------------------------
							Public member variables
	-------------------------------------------------------------------------*/
	// The centre of the circle.
	glm::vec2 m_centre;

	// The radius of the circle.
	float m_radius;
};

// A class which represents a Well game entity. As well has havinf a circle to
// define the bounding circle for a well, it also determines whether the well
// is open or not.
class Well : public Circle
{
public:
	// Well Constructor. Sets the bound circle, as well as initilaising the
	// well to the given open or close state.
	Well(const glm::vec2 a_centre,
		float a_radius,
		bool a_isOpen);

	// Destructor. Remove the timer.
	~Well(){ delete m_p_reopenTimer; }

	/*-------------------------------------------------------------------------
								Public Methods
	-------------------------------------------------------------------------*/

	// Determines whether the time to reopen has elapsed.
	bool TimeElapsed();

	// Elapses time by the given amount (in seconds).
	void AddTime(float a_time);

	// Resets the timer back to 0
	void Reset();

	/*-------------------------------------------------------------------------
							Public member variables
	-------------------------------------------------------------------------*/
	// Whether the well is open or not.
	bool m_isOpen;

	/*-------------------------------------------------------------------------
							Private member variables
	-------------------------------------------------------------------------*/
private:
	// Timer for when to re-open the well.
	Timer *m_p_reopenTimer;
};
