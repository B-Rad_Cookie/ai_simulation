/*-----------------------------------------------------------------------------
file:			BlackSlime.h
author:			Brad Cook
description:	This header file contains the declaration of the BlackSlime
				class: The class which represents the Black Slime agent.
-----------------------------------------------------------------------------*/

#pragma once

// Forward Declares
class StateMachine;
class EntityManager;
class Graph;

// Includes
#include "Agent.h"

// Represents the Black Slime agent. Controls al the relevant information
// relating to a BlackSLime game object.
class BlackSlime : public Agent
{
public:
	// Constructor to set up a new Black Slime/
	BlackSlime(EntityManager *a_p_entities,
			   Graph *a_p_worldGraph);

	// Constructor to Setup a new Black SLime with Vector2 starting position.
	BlackSlime(EntityManager *a_p_entities,
		Graph *a_p_worldGraph,
		const glm::vec2 &a_position);

	//Destructor
	~BlackSlime();

	/*-------------------------------------------------------------------------
									Public Methods
	-------------------------------------------------------------------------*/

	// Update the BlackSlime information.
	void Update(EntityManager *a_p_entities,
				Graph *a_p_worldGraph,
				float deltaTime);

	/*-------------------------------------------------------------------------
							Private Member Variables
	-------------------------------------------------------------------------*/
	// The state machine for the Black SLime.
	StateMachine *m_p_stateMachine;
};